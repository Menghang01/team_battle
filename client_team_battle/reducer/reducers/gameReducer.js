
const gameReducer = (state = { game: null }, action) => {
    // console.log("object");
    console.log(action.payload)
    switch (action.type) {
        case "CREATE_GAME":
            return { ...state, game: action.payload }
        case "STORE_GAME":
            return {
                ...state,
                current_game: action.payload
            }
        default:
            return state
    }
}

export {
    gameReducer
}
