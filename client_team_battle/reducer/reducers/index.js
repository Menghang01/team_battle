import { combineReducers } from "redux";
import { reducer } from "./socketReducer"
import { gameReducer } from "./gameReducer"

const reducers = combineReducers({
    socket: reducer,
    game: gameReducer,
});

export default reducers;
