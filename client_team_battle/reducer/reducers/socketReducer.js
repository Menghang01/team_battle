
const reducer = (state = { socket: null }, action) => {
    // console.log("object");
    // console.log(action.payload)
    switch (action.type) {
        case "CREATE_SOCKET":
            return { ...state, socket: action.payload }
        default:
            return state
    }
}

export {
    reducer
}
