
export const createGame = (data) => async (dispatch) => {
    try {
        dispatch({ type: "CREATE_GAME", payload: data })
    } catch (error) {
        console.log(error)
    }
}


export const storeGame = (data) => async (dispatch) => {
    try {
        console.log(data);
        dispatch({ type: "STORE_GAME", payload: data })
    } catch (error) {
        console.log(error)
    }
}