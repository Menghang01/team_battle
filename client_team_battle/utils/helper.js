const getCookie = (cookie) => {
    cookie = cookie.split('; ');
    let result = {};
    for (let i in cookie) {
        const cur = cookie[i].split('=');
        result[cur[0]] = cur[1];
    }
    return result


}

export {
    getCookie
}