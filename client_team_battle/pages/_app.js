import '../styles/globals.css'
import { useDispatch } from "react-redux"
import { createWrapper } from "next-redux-wrapper";
import { Provider } from "react-redux";
import io from "socket.io-client"
import { useEffect } from "react"
import store from "../reducer/store"
import { createSocket } from '../action/socketAction';
import axios from 'axios'
import { socket } from "../socket"
axios.defaults.baseURL = process.env.NODE_ENV === "development" ? "http://localhost:8000" : "http://139.59.101.71:8000"

function MyApp({ Component, pageProps }) {

  const dispatch = useDispatch()

  useEffect(() => {
    console.log(socket)
    dispatch(createSocket(socket))

  }, [dispatch])


  return (
    <Provider store={store}>
      <Component {...pageProps} />

    </Provider>
  )


}

const makestore = () => store;
const wrapper = createWrapper(makestore);

export default wrapper.withRedux(MyApp);

