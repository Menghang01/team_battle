import { useRouter } from 'next/router'
import React from 'react'
import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import GrimReaper from '../components/lottie/grim-reaper'

const TeamDied = () => {
    const router = useRouter()
    const socket = useSelector(state => state.socket.socket)

    useEffect(() => {
        if (socket) {
            socket.on("hi", (state) => {
                console.log("hi");
            })
            socket.on("game-stat", (state) => {
                console.log("Team died")
                localStorage.setItem("team_stats", JSON.stringify(state))

                setTimeout(() => {
                    localStorage.setItem("team_stats", JSON.stringify(state))

                }, 300);


                router.push({
                    pathname: '/leaderboad',
                    query: {
                        stats: JSON.stringify(state)
                    }
                }, '/leaderboard')


            })

            socket.on("end-game-stats", (state) => {
                router.push({
                    pathname: '/leaderboad',
                    query: {
                        stats: JSON.stringify(state)
                    }
                }, 'leaderboard')
            })


            socket.on("team-all-dead", (state) => {
                localStorage.setItem("team_stats", JSON.stringify(state))

                setTimeout(() => {
                    localStorage.setItem("team_stats", JSON.stringify(state))

                }, 300);
                router.push({
                    pathname: '/leaderboard',
                    query: {
                        stats: JSON.stringify(state)
                    }

                }, 'leaderboard')



            })


        }

    }, [socket])

    // const emit = () => {
    //     socket.emit("test")
    // }


    return (
        <div className='w-screen h-screen bg-[#bd5f48] flex flex-col items-center justify-center'>
            {/* <button onClick={emit}>sd</button> */}
            <GrimReaper />
            <h6 className='font-bold text-lg sm:text-2xl text-white'>Your team has been defeated</h6>
            <p className='mt-4 text-sm sm:text-base text-white'>Please wait for the game to finish</p>
        </div>
    )
}

export default TeamDied