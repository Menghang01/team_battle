import React, { useEffect } from 'react'
import { useDispatch, useSelector } from "react-redux"
import { createGame } from '../../action/gameAction'
import Router, { useRouter } from 'next/router'
const SelectGameMode = () => {
    const dispatch = useDispatch()
    const socket = useSelector(state => state.socket.socket)
    const game = useSelector(state => state.game.game)

    const router = useRouter()

    useEffect(() => {
        console.log(game);
        if (game === null) {
            router.push('/teacher/games')
        }
    }, [])

    const chooseGame = (mode) => {
        dispatch(createGame({
            ...game,
            game_mode: mode
        }))

        Router.push("/team-setting")
    }
    return (
        <div className='main'>
            <div className="background w-screen h-screen bg-primary-600 relative overflow-hidden -z-0">
                <img className='w-4/5 h-4/5 absolute -bottom-64 -right-96 z-1' src="/assets/radio_ring.svg" alt="doodle" />
                <h6 className='pl-5 pt-5 font-bold text-white text-[1.5rem]'>LOGO</h6>
                <div className="absolute w-full h-full flex flex-col items-center mt-10 z-10">
                    <h6 className='font-medium text-white text-[1.2rem] sm:text-[1.5rem]'>Select game mode</h6>
                    <p className='text-white mt-2 text-[.8rem] xs:text-[.9rem] sm:text-[1rem]'>Select a game mode below for your audience to play</p>
                    <div className="select-game-mode flex flex-col sm:flex-row px-4 sm:px-0 w-full sm:w-1/2 lg:w-1/4 my-10 ">
                        <div onClick={() => chooseGame("boss")} className="game-mode-card mb-8 sm:mb-0 py-4 px-4 bg-white rounded-md w-full mr-10 flex flex-row sm:flex-col justify-center items-center hover:scale-105 hover:cursor-pointer transition-all">
                            <img className='w-28 h-36 sm:w-32 sm:h-44 mb-0 sm:mt-4 object-cover' src="/assets/boss.png" />
                            <div className="h-44 px-6 flex flex-col justify-center items-center">
                                <h6 className='font-bold text-[1rem] xs:text-[1.2rem] mb-5 text-center'>Teams vs Boss</h6>
                                <p className="description text-[.75rem] xs:text-[0.9rem] text-[#A4A4A4] text-center">
                                    The teams battle against each other to defeat the Sphinx boss. The team that kills the boss first wins.
                                </p>
                            </div>
                        </div>
                        {/* <div onClick={() => chooseGame("team")} className="game-mode-card py-5 px-4 bg-white rounded-md w-full flex flex-row sm:flex-col justify-center items-center hover:scale-105 hover:cursor-pointer transition-all">
                            <img className='w-28 h-36 sm:w-full sm:h-44 mb-0 sm:mb-8 object-cover rounded-md' src="/assets/versus.png" />
                            <div className="h-44 px-6 flex flex-col justify-center items-center">
                                <h6 className='font-bold text-[1rem] xs:text-[1.2rem] mb-5 text-center'>Team vs Team</h6>
                                <p className="description text-center text-[.75rem] xs:text-[0.9rem] text-[#A4A4A4]">
                                    The teams battle against each other and the team that loses all their HP first will be defeated.
                                </p>
                            </div>
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SelectGameMode


export async function getServerSideProps({ req, res }) {
    
    return {
        props: {}, // will be passed to the page component as props
    }
}
