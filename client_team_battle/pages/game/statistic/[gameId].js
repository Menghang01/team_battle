import axios from 'axios'
import React, { useState, useEffect } from 'react'
import Cookies from "cookies-js"
import { getCookie } from '../../../utils/helper'
import Router from 'next/router'

const Leaderboard = ({ game }) => {

    const [info, setInfo] = useState(game)



    const [top, setTop] = useState()


    useEffect(() => {
        if (info) {
            console.log(info);
            var data = info.teamStatistic,
                sorted = {};

            console.log(data);

            Object
                .keys(data).sort(function (a, b) {
                    return data[b].totalDamageInflicted - data[a].totalDamageInflicted;
                })
                .forEach(function (key) {
                    sorted[key] = data[key];
                });

            var result = Object.keys(sorted).map((key) => [key, sorted[key]]);
            console.log(sorted);
            console.log(result[0][0]);
            setTop([
                ...result
            ])
        }

    }, [info]);

    const push = () => {
        if (Cookies.get("role") === "Teacher") {
            Router.replace("/teacher")
        } else {
            Router.replace("/join-quiz")
        }
    }



    return (

        info !== undefined && top !== undefined ? < div className="main" >
            <div className='leaderboard w-screen h-screen bg-[#F6F8FF] relative overflow-x-hidden' id='customscroll'>
                <img className='w-3/5 h-3/5 absolute -top-5 -left-64 opacity-10' src="/assets/ring.svg" alt="doodle" />
                <img className='w-3/5 h-3/5 absolute -bottom-5 -right-64 opacity-10' src="/assets/hexa.svg" alt="doodle" />
                <h6 onClick={push} className='hover:cursor-pointer  absolute pl-9 pt-5 font-bold text-primary-600 text-[1.8rem]'>HOME</h6>
                <div className="top-performer mt-16">
                    <div className="top-performing w-full h-[28rem] flex flex-col items-center">
                        <h6 className='font-bold text-[1.2rem] mt-8 mb-20'>Top Performing Team</h6>
                        <div className="ranking relative bg-blue-400 scale-90">
                            {
                                top.length >= 2 ?
                                    <div className='second-place absolute -left-[8rem] -bottom-[8.7rem] sm:-left-56 sm:-bottom-16'>
                                        <div className="relative flex justify-center">
                                            <div className="absolute first-background bg-[#7384F6] rounded-lg rounded-br-none rounded-tr-none w-28 h-48 sm:w-52 sm:h-64 flex flex-col justify-between items-center pt-20 pb-10">
                                                {/* <h6 className='text-white font-bold text-[1.2rem] sm:text-[1.5rem]'>{top[1][0]}</h6> */}
                                                <div className="stats-bottom flex flex-col items-center">
                                                    <h6 className='text-[#FFC700] font-bold text-[1rem] sm:text-[1.8rem] mb-2'>{top[1][1].totalDamageInflicted}</h6>

                                                </div>
                                            </div>
                                            <div className="absolute -top-8 sm:-top-16 w-16 h-16 sm:w-28 sm:h-28 bg-[#BEB8AE] rounded-full flex justify-center items-center">
                                                <h6 className='font-bold text-white text-[2rem] sm:text-[4rem]'>2</h6>
                                            </div>
                                        </div>
                                    </div> : null

                            }


                            {
                                top.length >= 3 ? <div className='third-place absolute -right-[8rem] -bottom-[8.7rem] sm:-right-56 sm:-bottom-16'>
                                    <div className="relative flex justify-center">
                                        <div className="absolute first-background bg-[#7384F6] rounded-lg rounded-bl-none rounded-tl-none w-28 h-48 sm:w-52 sm:h-64 flex flex-col justify-between items-center pt-20 pb-20 sm:pb-10">
                                            <h6 className='text-white font-bold text-[1.2rem] sm:text-[1.5rem]'>{top[1][0]}</h6>
                                            {/* <h6 className='text-white font-bold text-[1.2rem] sm:text-[1.5rem]'>{team[0].team_name}</h6> */}
                                            <div className="stats-bottom flex flex-col items-center">
                                                <h6 className='text-[#FFC700] font-bold text-[1rem] sm:text-[1.8rem] mb-2'>2823 Points</h6>
                                                {/* <h6 className='text-[#FFC700] font-bold text-[1rem] sm:text-[1.8rem] mb-2'>{team[0].score + "Points"}</h6> */}
                                                {/* <p className='text-white font-medium text-[.8rem] sm:text-[1rem] text-center'>{team[0].duration}</p> */}
                                                <p className='text-white font-medium text-[.8rem] sm:text-[1rem] text-center'>4 minutes 23 seconds</p>
                                            </div>
                                        </div>
                                        <div className="absolute -top-8 sm:-top-16 w-16 h-16 sm:w-28 sm:h-28  bg-[#F3A93A] rounded-full flex justify-center items-center">
                                            <h6 className='font-bold text-white text-[2rem] sm:text-[4rem]'>3</h6>
                                        </div>
                                    </div>
                                </div> : null
                            }

                            <div className='first-place absolute -bottom-[4.7rem] sm:bottom-0'>
                                <div className="relative flex justify-center">
                                    <div className="absolute first-background bg-[#495EE9] rounded-lg rounded-bl-none rounded-br-none w-36 h-64 sm:w-60 sm:h-80 flex flex-col justify-between items-center pt-20 pb-10">
                                        <h6 className='text-white font-bold text-[1.5rem]'>{top[0][0]}</h6>
                                        {/* <h6 className='text-white font-bold text-[1.5rem]'>{team[0].team_name}</h6> */}
                                        <div className="stats-bottom flex flex-col items-center">
                                            <h6 className='text-[#FFC700] font-bold text-[1.2rem] sm:text-[1.8rem] mb-2'>{top[0][1].totalDamageInflicted}</h6>
                                            {/* <h6 className='text-[#FFC700] font-bold text-[1.2rem] sm:text-[1.8rem] mb-2'>{team[0].score + "Points"}</h6> */}
                                            {/* <p className='text-white font-medium text-[.8rem] sm:text-[1rem]'>4 minutes 23 seconds</p> */}
                                            {/* <p className='text-white font-medium text-[.8rem] sm:text-[1rem]'>{team[0].duration}</p> */}
                                        </div>
                                    </div>
                                    <div className="absolute -top-12 sm:-top-16 w-20 h-20 sm:w-32 sm:h-32 bg-[#FFC700] rounded-full flex justify-center items-center">
                                        <h6 className='font-bold text-white text-[2.5rem] sm:text-[4.5rem]'>1</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="team-leaderboard">
                    <div className="team-leaderboard w-full px-4 mb-8 h-fit flex flex-col items-center">
                        <h6 className='font-bold text-[1.2rem] mb-6'>Team Leaderboard</h6>
                        <div className="grid grid-cols-1 xs:grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 gap-4">
                            {
                                info && Object.entries(info.teamStatistic).map(([key, value], index) => {
                                    return (
                                        <div className={`team-card text-black p-4 bg-[#7384F6] rounded-md box-border`}>
                                            <div className='border-b-2 border-grey-500 pb-3 mb-5 flex justify-between'>
                                                <h6 className='font-bold text-white'>{key}</h6>
                                                <h6 className='font-bold text-white'>{info.teamStatistic[key].totalDamageInflicted}</h6>
                                            </div>
                                            <div className="member-list flex flex-wrap text-white">
                                                {
                                                    info && info.team[key].map((member, index) => {
                                                        return (
                                                            <div className="member-name bg-white mb-2 mr-2 py-2 px-4 rounded-md hover:cursor-pointer hover:scale-105 transition-all" key={index}>
                                                                <p className='text-[#7384F6] font-medium'>{member.userName}</p>
                                                            </div>
                                                        )
                                                    })
                                                }
                                                <div className="w-10"></div>
                                            </div>


                                        </div>
                                    )
                                })
                            }


                        </div>
                    </div>
                </div>

            </div>
        </div > : null
    )




}

export default Leaderboard

export async function getServerSideProps({ req, res, params }) {
    let cookie, role

    if (req.headers.cookie !== undefined) {
        cookie = getCookie(req.headers.cookie)
        role = cookie.role
    } else {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }


    console.log(params.gameId);
    const response = await axios.get("/api/game/" + params.gameId, {
        headers: {
            'authorization': `Bearer ${cookie.accessToken}`
        }
    })
    console.log(response.data);


    return {
        props: {
            game: response.data
        }, // will be passed to the page component as props
    }
}
