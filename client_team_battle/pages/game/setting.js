import React from 'react';

const Setting = () => {
    return (
        <div className='main'>
            <div className="background w-screen h-screen bg-primary-600 relative overflow-hidden -z-0">
                <img className='w-4/5 h-4/5 absolute -bottom-64 -left-96 z-1' src="/assets/radio_ring.svg" alt="doodle" />
                <h6 className='pl-5 pt-5 font-bold text-white text-[1.5rem]'>LOGO</h6>
                <div className="absolute w-full h-full flex flex-col items-center mt-10 z-10">
                    <h6 className='font-medium text-white text-[1.5rem]'>Team Setting</h6>
                    <p className='text-white mt-2'>Select a game mode below for your audience to play</p>
                    <div className="select-game-mode flex w-5/12 space-x-10 my-10">
                        <div className="game-mode-card py-8 px-8 bg-white rounded-md flex flex-col justify-center items-center hover:scale-105 hover:cursor-pointer transition-all">
                            <img className='w-32 mb-8' src="assets/boss.png" />
                            <h6 className='font-bold text-[1.2rem] mb-5'>Teams vs Boss</h6>
                            <p className="description text-center text-[0.9rem] text-[#A4A4A4]">
                                The teams battle against each other to defeat the Sphinx boss. The team that kills the boss first wins.
                            </p>
                        </div>
                        <div className="game-mode-card py-8 px-8 bg-white rounded-md flex flex-col justify-center items-center hover:scale-105 hover:cursor-pointer transition-all">
                            <img className='mb-8' src="assets/versus.png" />
                            <h6 className='font-bold text-[1.2rem] mb-5'>Team vs Team</h6>
                            <p className="description text-center text-[0.9rem] text-[#A4A4A4]">
                                The teams battle against each other and the team that loses all their HP first will be defeated.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Setting;
