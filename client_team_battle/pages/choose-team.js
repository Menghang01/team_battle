import React, { useEffect, useState } from 'react'
import PulseLoading from '../components/lottie/pulse'
import WaitingPigeon from '../components/lottie/waiting-pigeon'
import TeamCard from '../components/team_card'
import Router, { useRouter } from "next/router"
import { useDispatch, useSelector } from 'react-redux'
import { storeGame } from '../action/gameAction'

const ChooseTeam = () => {
    // current user information
    const [userInfo, setUserInfo] = useState()

    useEffect(() => {
        var retrievedUserInfo = localStorage.getItem('userInfo');
        console.log(JSON.parse(retrievedUserInfo))

        setUserInfo(JSON.parse(retrievedUserInfo))
    }, [])

    const router = useRouter()
    const socket = useSelector(state => state.socket.socket)
    const [team, setTeam] = useState(router.query.team_name)
    useEffect(() => {
        setUserInfo(JSON.parse(localStorage.getItem("userInfo")))
        if (team) {
            localStorage.setItem("team", team)
        } else {
            setTeam(localStorage.getItem("team"))
        }



    }, []);
    const dispatch = useDispatch()

    useEffect(() => {
        if (socket) {
            socket.on("move-to-game-page", (game) => {
                dispatch(storeGame(game))
                console.log(game)

                let team_name
                let userInfo = JSON.parse(localStorage.getItem("userInfo"))

                Object.keys(game.team).map((team) => {
                    let found = game.team[team].some((member) => member._id == userInfo._id)

                    if (found) {
                        team_name = team
                    }
                })
                
                router.push({
                    pathname: "/play?id=" + game._id,
                    query: {
                        quesionIndex: 0,
                        team: localStorage.getItem("team")

                    }
                }, "/play?id=" + game._id + "&team=" + team_name)
            })
        }
    }, [socket]);

    const [screenWidth, setScreenWidth] = useState({
        winWidth: undefined,
    })
    function detectSize() {
        setScreenWidth({
            winWidth: window.innerWidth,
        })
    }

    // useEffect(() => {
    //     if (typeof window !== 'undefined') {

    //         window.addEventListener('resize', detectSize)

    //         detectSize();

    //         return () => {
    //             window.removeEventListener('resize', detectSize)
    //         }
    //     }
    // }, [screenWidth])


    useEffect(() => {
        if (socket) {
            socket.on("manually-added-successfully", (user, selected_team) => {

                // console.log(user._id, userInfo._id, user._id === userInfo._id);
                if (user._id === JSON.parse(localStorage.getItem("userInfo"))._id) {

                    setTeam(selected_team)

                    localStorage.setItem("team", selected_team)
                }

            })
        }
    }, [socket]);

    return (
        <div className='h-screen relative overflow-hidden bg-primary-600'>
            <div className="absolute -bottom-[28rem] -right-[28rem] lg:-bottom-[30rem] lg:-right-[30rem]">
                <PulseLoading screenWidth={screenWidth} />
            </div>
            <div className="absolute grid grid-rows-6 sm:grid-rows-none sm:grid-cols-6 lg:grid-cols-4 h-full">
                <div className="doodle bg-white h-full w-full row-span-6 sm:col-span-2 lg:col-span-1">
                    {/* <Image src="/assets/doodle.svg" layout='fill' sizes='100%'/> */}
                    <img className='w-full h-full object-cover opacity-80' src="/assets/doodle.svg" alt="doodle" />
                </div>
                <div className="form h-full w-full row-span-6 sm:col-span-4 lg:col-span-3 text-white p-12 flex flex-col justify-between overflow-y-hidden" id='customscroll'>
                    <div className="upper">
                        <div className="form-head font-bold mb-16">
                            <div className="form-welcome flex items-center space-x-3">
                                <img className='h-10 w-10 ' src="/assets/waving-hand.png" alt="waving" />
                                <h6 className='text-[1.5rem] lg:text-[2rem]'>Welcome, {userInfo && userInfo.firstName}</h6>
                            </div>
                            <p className='pt-2 text-[1rem] font-thin'>Are you ready for battle?</p>
                        </div>
                        <div className="waiting-content">
                            <h6 className='text-[1.8rem] lg:text-[2.2rem] font-bold'>Congrats! You're in!</h6>
                            <p className='pt-2 text-[.9rem] lg:text-[1.1rem] font-thin'>Please wait for the game to start...</p>
                            <div className="mt-10">
                                <WaitingPigeon screenWidth={screenWidth} />
                            </div>
                        </div>
                    </div>
                    <div className="logo flex justify-center items-end mt-6">
                        <h6 className='font-bold text-[1.8rem]'>LOGO</h6>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default ChooseTeam