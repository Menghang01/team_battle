import React, { useState, useEffect } from 'react'
import TeamCard from '../components/team_card'
import { storeGame } from "../action/gameAction"
import { useDispatch, useSelector } from 'react-redux'
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Popover } from '@headlessui/react';
import Router from "next/router"
import { useRouter } from 'next/router'
import { setLocale } from 'yup';
import SandLoading from '../components/lottie/loading';

const TeamSelection = () => {
    const router = useRouter()
    const dispatch = useDispatch()
    const socket = useSelector(state => state.socket.socket)

    const currentGame = useSelector((state) => state.game.current_game)
    const [nameList, setNameList] = useState([

    ])
    const [teamList, setTeamList] = useState()

    useEffect(() => {
        console.log("DOG");
        if (currentGame === undefined) {
            Router.push('/teacher')
        }

        const tempTeam = []

        console.log(currentGame);

        Object.keys(currentGame["team"]).map((team_name, index) => {
            const temp = {
                team_name: team_name,
                team_members: [],
            }

            tempTeam.push(temp)
        })

        setTeamList(tempTeam)
    }, [])


    useEffect(() => {
        console.log("CURRENT_GAME")
        console.log(currentGame)
    }, [currentGame]);

    const [winReady, setwinReady] = useState(false);
    useEffect(() => {
        setwinReady(true);
    }, []);

    useEffect(() => {
        if (socket) {
            socket.on("player-added", (isLive, team, team_name) => {

                currentGame["team"] = team
                dispatch(storeGame({
                    ...currentGame
                }))

                let tempTeam = []

                Object.keys(currentGame["team"]).map((team_name, index) => {
                    const temp = {
                        team_name: team_name,
                        team_members: currentGame["team"][team_name],
                    }

                    tempTeam.push(temp)

                    console.log(team_name, currentGame["team"][team_name]);
                })
                setTeamList(tempTeam)
                console.log(currentGame);

            })

            socket.on("manual-mode-init-game", () => {
                console.log("manual")
                setNameList([])
            })

            socket.on("player-added-manually", (players_team) => {
                setNameList([...players_team])
            })
        }
    }, [socket]);

    const [loading, setLoading] = useState(false)

    const startGame = () => {
        socket.emit("start-game", currentGame.quizId)
        setLoading(true)
        router.push(`/play?id=${currentGame._id}`)
    }

    const move1 = (source, destination, droppableSource, droppableDestination) => {
        const sourceClone = Array.from(source);
        const destClone = Array.from(destination);
        const [removed] = sourceClone.splice(droppableSource.index, 1);

        const destIndex = destClone.findIndex((el) => el.team_name === droppableDestination.droppableId)
        destClone[destIndex].team_members.splice(droppableDestination.index, 0, removed);

        const result = {}

        result[droppableSource.droppableId] = sourceClone;
        result[droppableDestination.droppableId] = destClone;

        return result;
    };

    const move2 = (source, destination, droppableSource, droppableDestination) => {
        const sourceClone = Array.from(source);
        const destClone = Array.from(destination);
        // const [removed] = sourceClone.splice(droppableSource.index, 1);

        const sourceIndex = sourceClone.findIndex((el) => el.team_name === droppableSource.droppableId)
        const [removed] = sourceClone[sourceIndex].team_members.splice(droppableSource.index, 1);
        destClone.splice(droppableDestination.index, 0, removed);

        const result = {}

        result[droppableSource.droppableId] = sourceClone;
        result[droppableDestination.droppableId] = destClone;

        return result;
    };

    function onDragEnd(result) {
        console.log("Drag end");
        const { source, destination, draggableId } = result;

        console.log({ source, destination, draggableId });

        if (!destination) {
            return
        }

        if (destination.droppableId == source.droppableId && destination.index === source.index) {
            return
        }

        console.log(nameList[source.index], destination.droppableId)

        if (destination.droppableId !== source.droppableId) {
            if (source.droppableId === "student_waiting") {
                const result = move1(nameList, teamList, source, destination);
                setNameList(result.student_waiting)
                console.log()
                console.log(result.student_waiting)
            } else if (destination.droppableId === "student_waiting") {
                const result = move2(teamList, nameList, source, destination);
                setNameList(result.student_waiting)
                console.log(result.student_waiting)

            } else {
                return
            }

        }

        socket.emit("add-player-manually", nameList[source.index], destination.droppableId)
    }



    return (
        currentGame &&
        <div className="main select-none">
            <div className="background w-screen h-screen bg-primary-600 relative overflow-y-scroll overflow-x-hidden" id='customscroll'>
                <img className='w-4/5 h-4/5 absolute -bottom-1/3 -left-1/3 z-1' src="/assets/radio_ring.svg" alt="doodle" />
                <h6 className='pl-5 pt-5 font-bold text-white text-[1.5rem]'>LOGO</h6>
                <div className="login-form w-screen h-screen absolute flex flex-col items-center justify-start">
                    <div className="join-code mb-8">
                        <h6 className='text-white font-bold text-[1.1rem] mb-4'>Enter the join code</h6>
                        <div className="join-code-container flex justify-center items-center bg-white rounded-md py-2 scale-110">
                            <h2 className='font-bold text-[1.8rem]'>{currentGame.pin}</h2>
                        </div>
                    </div>
                    {
                        winReady &&
                        <DragDropContext onDragEnd={onDragEnd}>
                            {
                                currentGame.allocation_setting === "auto"
                                    ? null
                                    : <div className="student-name flex-col items-start w-full px-5">
                                        <h6 className='font-bold text-white text-base mb-4'>Student names</h6>
                                        <div className="bg-white w-full rounded-lg px-6 pt-6 pb-4">
                                            {/* <div className="stats-row flex space-x-8 mb-4">
                                                <div className="total-students flex space-x-2">
                                                    <ion-icon name="people-outline" id="people-outline2"></ion-icon>
                                                    <p className='font-medium'>Total students: 12</p>
                                                </div>
                                                <div className="not-in-team flex space-x-2">
                                                    <ion-icon name="people-outline" id="people-outline2"></ion-icon>
                                                    <p className='font-medium'>Not in team: 6</p>
                                                </div>
                                            </div> */}
                                            <Droppable droppableId="student_waiting" direction='horizontal'>
                                                {(provided) => (
                                                    <div
                                                        className="name-list-row flex flex-wrap"
                                                        {...provided.droppableProps}
                                                        ref={provided.innerRef}
                                                    >
                                                        {nameList.length !== 0 
                                                        ? nameList.map((name, index) => {
                                                            return (
                                                                <Draggable key={name.userName} draggableId={name.userName} index={index}>
                                                                    {(provided) => (
                                                                        <div
                                                                            className='bg-primary-400 text-white mr-2 mb-2 py-2 px-4 rounded-md hover:cursor-pointer hover:scale-105 transition-all'
                                                                            {...provided.draggableProps}
                                                                            {...provided.dragHandleProps}
                                                                            ref={provided.innerRef}
                                                                        >
                                                                            <p>{name.firstName}</p>
                                                                        </div>
                                                                    )}
                                                                </Draggable>
                                                            )
                                                        })
                                                        : <p className='text-grey-600'>No one has joined the game yet</p>}
                                                        {provided.placeholder}
                                                    </div>
                                                )}
                                            </Droppable>
                                            <div className="team-list-row"></div>
                                        </div>
                                    </div>
                            }
                            <div className="team flex-col items-start w-full px-5 ">
                                <h6 className='font-bold text-white text-base my-4'>Teams</h6>
                                <div className="team-list grid grid-cols-1 xs:grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 gap-8 pr-4" id='customscroll'>
                                    {/* <div className={`team-card text-black p-4 rounded-md box-border border-dashed border-2 border-white flex items-center justify-center transition-all hover:cursor-pointer hover:border-solid hover:bg-white hover:bg-opacity-20 hover:backdrop-blur-sm`}>
                                        <div className='flex justify-center items-center space-x-2 hover:scale-105 transition-all'>
                                            <ion-icon name="add-circle-outline" id="add-circle-outline"></ion-icon>
                                            <h6 className='font-bold text-white'>Add Team</h6>
                                        </div>
                                    </div> */}
                                    {
                                        teamList && teamList.map((team, index) => {
                                            return (
                                                <TeamCard teamInfo={{
                                                    team_name: team.team_name,
                                                    team_members: team.team_members
                                                }} key={index} />
                                            )
                                        })
                                    }
                                    {/* 
                                    {

                                        currentGame["team"] ? Object.keys(currentGame["team"]).map((team_name, index) => {
                                            return (
                                                <TeamCard teamInfo={{
                                                    team_name: team_name,
                                                    team_members: currentGame["team"][team_name]
                                                }
                                                } key={team_name} />
                                            )
                                        }
                                        ) : null
                                    } */}

                                </div>
                            </div>
                        </DragDropContext>
                    }
                    {
                        !loading 
                        ? <div onClick={startGame} className='text-primary-400 mt-20 py-2 px-6 rounded-md font-bold bg-white w-1/10 h-1/10 hover:cursor-pointer hover:border hover:border-white hover:bg-primary-600 hover:text-white transition-all'>Start game</div>
                        : <div className="mt-14">
                            <SandLoading />
                        </div>
                    }
                    {/* <div onClick={nextQuestion} className='text-white bg-black w-1/10 h-1/10'>Next question</div> */}
                </div>

            </div>
        </div>
    )
}

export default TeamSelection