import React from 'react'

const Error404 = () => {
    return (
        <div className='w-screen h-screen bg-primary-400 flex justify-center items-center'>
            <h6 className='font-bold text-[2rem] text-white'>404: Nothing to show here</h6>
        </div>
    )
}

export default Error404