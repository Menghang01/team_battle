import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import io from 'socket.io-client';
import { getCookie } from '../utils/helper';

import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';
import WaitingPigeon from '../components/lottie/waiting-pigeon';
import { Router, useRouter } from 'next/router';
export default function Home() {
  const router = useRouter()

  const socket = []
  // const socket = useSelector((state) => state.socket.socket)

  const initGame = () => {
    socket.emit("init-game", { pin: 1, _id: "games" }, "hi")
  }

  const joinRoom = () => {
    socket.emit("add-player", { userName: "menghang", _id: 1 }, socket.id, 1, (a, b, c) => { })
  }
  return (
    <div className="welcome h-screen w-screen bg-primary-600 flex items-center justify-center">
        <div className="flex flex-col items-center ">
          <h6 className='text-white font-bold text-[2rem] mb-8'>Welcome to Team Battle</h6>
          <WaitingPigeon />
          <div onClick={() => router.push('/log-in')} className="submit bg-white text-[.75rem] sm:text-[.9rem] mt-8 px-5 py-2 rounded-md hover:box-border text-primary-600 transition-all hover:cursor-pointer hover:scale-105">
              <p className='font-bold'>Log in</p>
          </div>
        </div>
    </div>
  )
}

export async function getServerSideProps({ req, res }) {
  let cookie, role

  if (req.headers.cookie !== undefined) {
      cookie = getCookie(req.headers.cookie)
      role = cookie.role
  } else {
      return {
          redirect: {
              destination: '/log-in',
              permanent: false
          },
      }
  }

  if (role !== "Teacher" ) {
      return {
          redirect: {
              destination: '/join-quiz',
              permanent: false
          },
      }
  } else {
    return {
      redirect: {
          destination: '/teacher',
          permanent: false
      },
  }
  }

  return {
      props: {}, // will be passed to the page component as props
  }
}