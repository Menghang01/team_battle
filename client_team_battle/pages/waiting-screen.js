


import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';
import { withRouter } from 'next/router'
import Router from 'next/router'

const WaitingScreen = (props) => {
    const socket = useSelector((state) => state.socket.socket)

    const [team, setTeam] = useState({})

    const [index, setIndex] = useState(0)

    useEffect(() => {
        if (props.router.query && props.router.query.team) {

            let temp = JSON.parse(props.router.query.team)
            setTeam({
                ...temp
            })

        } else {
            Router.push("/join-quiz")
        }

    }, [props.router.query]);

    useEffect(() => {
        if (socket != null) {

            socket.on("player-added", (data, team) => {
                console.log(data, team);
                setTeam({
                    ...team
                })
            })


            socket.on("next-questions", (index) => {
                console.log(index);
                setIndex(index)
            })

        }


    }, [socket]);

    useEffect(() => {
        console.log(team)
    }, [team])



    return (
        <div>
            {Object.entries(team).map((t, k) => {
                return (
                    <div>

                        < div> {t[0]}</div>
                        {
                            t[1].map(e => {
                                return (
                                    <p>{e.userName}</p>

                                )
                            })
                        }

                    </div>
                )



            }
            )}

            <p>{index}</p>

        </div >
    );
}

export default withRouter(WaitingScreen)
