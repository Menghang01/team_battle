import React, { useEffect, useState } from 'react'
import GameCard2 from '../../components/game_card2'
import TeacherLayout from '../../layout/teacher-layout'

import axios from "axios"
import { getCookie } from '../../utils/helper'
import { redirect } from 'next/dist/server/api-utils'
const YourGames = ({ quiz }) => {

    const mockYourGames = [
        {
            title: 'Title 1',
            date: 'April 27, 2022',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat...',
            imgUrl: 'https://wallpaper.dog/large/20393906.jpg'
        },
        {
            title: 'Title 2',
            date: 'May 15, 2022',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat...',
            imgUrl: 'https://wallpaperaccess.com/full/186639.jpg'
        },
        {
            title: 'Title 3',
            date: 'May 28, 2022',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat...',
            imgUrl: 'https://media.istockphoto.com/vectors/japanese-traditional-pattern-vector-id1194433695?b=1&k=20&m=1194433695&s=612x612&w=0&h=M3jpeVKQUUMk9tbKA7v--0IOckhRJUpAmTh3dQsl-vI='
        },
        {
            title: 'Title 4',
            date: 'June 18, 2022',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat...',
            imgUrl: 'https://wallpaper.dog/large/20393906.jpg'
        },
        {
            title: 'Title 1',
            date: 'April 27, 2022',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat...',
            imgUrl: 'https://wallpaper.dog/large/20393906.jpg'
        },
        {
            title: 'Title 2',
            date: 'May 15, 2022',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat...',
            imgUrl: 'https://wallpaperaccess.com/full/186639.jpg'
        },
        {
            title: 'Title 3',
            date: 'May 28, 2022',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat...',
            imgUrl: 'https://media.istockphoto.com/vectors/japanese-traditional-pattern-vector-id1194433695?b=1&k=20&m=1194433695&s=612x612&w=0&h=M3jpeVKQUUMk9tbKA7v--0IOckhRJUpAmTh3dQsl-vI='
        },
        {
            title: 'Title 4',
            date: 'June 18, 2022',
            desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at nunc est. Donec laoreet justo erat...',
            imgUrl: 'https://wallpaper.dog/large/20393906.jpg'
        },
    ]

    const [mock, setMock] = useState(quiz)
    useEffect(() => {
        console.log(quiz);
    }, []);
    // your draft array variables
    const [yourGames, setYourGames] = useState([])

    // fetch draft game from API
    // const fetchDrafts = async () => {
    //     const res = await axios.get('/quiz')
    //     setYourDrafts(res.data)
    //     console.log(res)
    // }

    // useEffect(() => {
    //     fetchDrafts()
    // }, [])

    return (
        <TeacherLayout page="/teacher/games">
            <div className="teacher-dashboard w-full h-full p-8 overflow-x-hidden overflow-y-scroll" id='customscroll'>
                <div className="dashboard-header flex justify-between items-center mb-10">
                    <div className="your-dashboard">
                        <h6 className='font-bold text-[1.5rem] text-primary-600'>Your Games</h6>
                        <p className='text-[.9rem] text-[#7B7B7B]'>List of your games that are ready to be played</p>
                    </div>
                </div>
                <div className="dashboard-content">
                    <div className="your-games mb-8">
                        {
                            quiz.length !== 0
                                ? <div className="game-card-list grid grid-cols-1 auto-rows-max sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-2">
                                    {
                                        quiz && quiz.map((game, index) => {
                                            return (
                                                <GameCard2 key={index} isDraft={true} game={game} />
                                            )
                                        })
                                    }
                                </div>
                                : <div className="w-full relative flex flex-col items-center justify-center">
                                    <img
                                        src="https://img.freepik.com/free-vector/nothing-here-flat-illustration_418302-77.jpg"
                                        alt="nothing-to-show"
                                        className='h-[10rem] w-[10rem] xs:h-[20rem] xs:w-[20rem] sm:h-[30rem] sm:w-[30rem] object-cover'
                                    />
                                    <h6 className='absolute -bottom-10 xs:bottom-0 sm:bottom-16 font-medium text-primary-600 text-center text-[.8rem] xs:text-[1rem] sm:text-lg'>You have not started any games yet</h6>
                                </div>
                        }
                    </div>
                </div>
            </div>
        </TeacherLayout>
    )
}

export default YourGames

export async function getServerSideProps({ req, res }) {
    let cookie
    let data
    if (req.headers.cookie) {
        cookie = getCookie(req.headers.cookie)

        const accessToken = cookie.accessToken
        if (!accessToken) {
            return {
                redirect: {
                    destination: '/log-in',
                    permanent: false
                },
            }
        }
        if (cookie.role !== "Teacher") {
            return {
                redirect: {
                    destination: '/join-quiz',
                    permanent: false
                },
            }
        }
        const response = await axios.get(`/api/quiz/teacher/${cookie.id}`, {
            headers: {
                'authorization': `Bearer ${cookie.accessToken}`
            }
        })

        data = response.data



    } else {
        return {
            redirect: {
                destination: '/log-in',
                permanent: false
            },
        }
    }











    return {
        props: {
            quiz: data
        }, // will be passed to the page component as props
    }


}
