import { useRouter } from 'next/router'
import React from 'react'
import TeacherLayout from '../../layout/teacher-layout'

const GameStatistic = () => {

    const router = useRouter()
    const { id } = router.query

    const mockTeamList = [
        {
            team_name: 'Spirit',
            team_members: [
                'Sivly',
                'Sivhour',
                'Sivming',
            ]
        },
        {
            team_name: 'Clone',
            team_members: [
                'Sivly',
                'Sivhour',
                'Sivming',
            ]
        },
        {
            team_name: 'Spirit',
            team_members: [
                'Sivly',
                'Sivhour',
                'Sivming',
            ]
        },
        {
            team_name: 'Clone',
            team_members: [
                'Sivly',
                'Sivhour',
                'Sivming',
            ]
        },
        {
            team_name: 'Clone',
            team_members: [
                'Sivly',
                'Sivhour',
                'Sivming',
            ]
        },
        {
            team_name: 'Spirit',
            team_members: [
                'Sivly',
                'Sivhour',
                'Sivming',
            ]
        },
        {
            team_name: 'Clone',
            team_members: [
                'Sivly',
                'Sivhour',
                'Sivming',
            ]
        },
    ]

    return (
        <TeacherLayout page="/teacher/games">
            <div className=" w-full h-full p-8 overflow-x-hidden overflow-y-scroll" id='customscroll'>
                <div className=" flex justify-between items-center mb-10">
                    <div className="">
                        <h6 className='font-bold text-[1.5rem] text-primary-600'>{id} Leaderboard</h6>
                        <p className='text-[.9rem] text-[#7B7B7B]'>View the statistics of this game</p>
                    </div>
                </div>
                <div className="statistic-content">
                    <div className="top-performing w-full h-[28rem] flex flex-col items-center">
                        <h6 className='font-bold text-[1.2rem] mb-20'>Top Performing Team</h6>
                        <div className="ranking relative bg-blue-400 scale-90">
                            <div className='second-place absolute -left-56 -bottom-16'>
                                <div className="relative flex justify-center">
                                    <div className="absolute first-background bg-[#7384F6] rounded-lg rounded-br-none rounded-tr-none w-52 h-64 flex flex-col justify-between items-center pt-20 pb-10"> 
                                        <h6 className='text-white font-bold text-[1.5rem]'>Blender</h6>
                                        <div className="stats-bottom flex flex-col items-center">
                                            <h6 className='text-[#FFC700] font-bold text-[1.8rem] mb-2'>2823 Points</h6>
                                            <p className='text-white font-medium'>4 minutes 23 seconds</p>
                                        </div>
                                    </div>
                                    <div className="absolute -top-16 w-28 h-28 bg-[#BEB8AE] rounded-full flex justify-center items-center">
                                        <h6 className='font-bold text-white text-[4rem]'>2</h6>
                                    </div>
                                </div>
                            </div>
                            <div className='third-place absolute -right-56 -bottom-16'>
                                <div className="relative flex justify-center">
                                    <div className="absolute first-background bg-[#7384F6] rounded-lg rounded-bl-none rounded-tl-none w-52 h-64 flex flex-col justify-between items-center pt-20 pb-10"> 
                                        <h6 className='text-white font-bold text-[1.5rem]'>Blender</h6>
                                        <div className="stats-bottom flex flex-col items-center">
                                            <h6 className='text-[#FFC700] font-bold text-[1.8rem] mb-2'>2823 Points</h6>
                                            <p className='text-white font-medium'>4 minutes 23 seconds</p>
                                        </div>
                                    </div>
                                    <div className="absolute -top-16 w-28 h-28 bg-[#F3A93A] rounded-full flex justify-center items-center">
                                        <h6 className='font-bold text-white text-[4rem]'>3</h6>
                                    </div>
                                </div>
                            </div>
                            <div className='first-place absolute'>
                                <div className="relative flex justify-center">
                                    <div className="absolute first-background bg-[#495EE9] rounded-lg rounded-bl-none rounded-br-none w-60 h-80 flex flex-col justify-between items-center pt-20 pb-10"> 
                                        <h6 className='text-white font-bold text-[1.5rem]'>Blender</h6>
                                        <div className="stats-bottom flex flex-col items-center">
                                            <h6 className='text-[#FFC700] font-bold text-[1.8rem] mb-2'>2823 Points</h6>
                                            <p className='text-white font-medium'>4 minutes 23 seconds</p>
                                        </div>
                                    </div>
                                    <div className="absolute -top-16 w-32 h-32 bg-[#FFC700] rounded-full flex justify-center items-center">
                                        <h6 className='font-bold text-white text-[4.5rem]'>1</h6>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div className="team-leaderboard w-full h-[28rem] flex flex-col items-center">
                        <h6 className='font-bold text-[1.2rem] mb-8'>Team Leaderboard</h6>
                        <div className="grid grid-cols-4 gap-4">
                            {
                                mockTeamList && mockTeamList.map((team, index) => {
                                    return (
                                        <div className={`team-card text-black p-4 bg-[#7384F6] rounded-md box-border`}>
                                            <div className='border-b-2 border-grey-500 pb-3 mb-5 flex justify-between'>
                                                <h6 className='font-bold text-white'>{team.team_name ?? "Unnamed"}</h6>
                                                <h6 className='font-bold text-white'>28232 pts</h6>
                                            </div>
                                            <div className="member-list flex text-white space-x-3">
                                                {
                                                    team.team_members && team.team_members.map((member_name, index) => {
                                                        return (
                                                            <div className="member-name bg-white py-2 px-4 rounded-md hover:cursor-pointer hover:scale-105 transition-all" key={index}>
                                                                <p className='text-[#7384F6] font-medium'>{member_name}</p>
                                                            </div>
                                                        )
                                                    })
                                                }
                                                <div className="w-10"></div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </TeacherLayout>
    )
}

export default GameStatistic