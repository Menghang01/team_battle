import React, { useEffect, useState } from 'react'
import axios from 'axios'
import GameCard from '../../components/game_card'
import TeacherLayout from '../../layout/teacher-layout'
import { getCookie } from '../../utils/helper'

const YourDrafts = () => {

    // your draft array variables
    const [yourDrafts, setYourDrafts] = useState([])

    // fetch draft game from API
    const fetchDrafts = async () => {
        const res = await axios.get('/api/quiz')
        setYourDrafts(res.data)
        console.log(res)
    }

    useEffect(() => {
        fetchDrafts()
    }, [])

    return (
        <TeacherLayout page="/teacher/drafts">
            <div className="teacher-dashboard w-full h-full p-8 overflow-x-hidden overflow-y-scroll" id='customscroll'>
                <div className="dashboard-header flex justify-between items-center mb-10">
                    <div className="your-dashboard">
                        <h6 className='font-bold text-[1.2rem] sm:text-[1.5rem] text-primary-600'>Your Drafts</h6>
                        <p className='text-[.8rem] sm:text-[.9rem] text-[#7B7B7B]'>List of your draft games</p>
                    </div>
                </div>
                <div className="dashboard-content">
                    <div className="your-games mb-8">
                        {
                            yourDrafts.length !== 0
                                ? <div className="game-card-list grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-2">
                                    {
                                        yourDrafts && yourDrafts.map((game, index) => {
                                            return (
                                                <GameCard key={index} isDraft={true} game={game} />
                                            )
                                        })
                                    }
                                </div>
                                : <div className="w-full relative flex flex-col items-center justify-center">
                                    <img
                                        src="https://img.freepik.com/free-vector/nothing-here-flat-illustration_418302-77.jpg"
                                        alt="nothing-to-show"
                                        className='h-[10rem] w-[10rem] xs:h-[20rem] xs:w-[20rem] sm:h-[30rem] sm:w-[30rem] object-cover'
                                    />
                                    <h6 className='absolute -bottom-10 xs:bottom-0 sm:bottom-16 font-medium text-primary-600 text-[.8rem] xs:text-[1rem] sm:text-lg'>You have not created any games yet</h6>
                                </div>
                        }
                    </div>
                </div>
            </div>
        </TeacherLayout>
    )
}

export default YourDrafts



export async function getServerSideProps({ req, res }) {
    let cookie = getCookie(req.headers.cookie)

    const accessToken = cookie.accessToken
    if (!accessToken) {
        return {
            redirect: {
                destination: '/log-in',
                permanent: false
            },
        }
    }

    if (cookie.role !== "Teacher") {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }



    return {
        props: {

        }
    }


}