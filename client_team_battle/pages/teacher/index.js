import React, { Children, useEffect, useState } from 'react'
import GameCard from '../../components/game_card'
import TeacherLayout from '../../layout/teacher-layout'
import Link from 'next/link'
import axios from 'axios'
import Cookies from "cookies-js"
import { useRouter } from 'next/router'
import { getCookie } from '../../utils/helper'

const TeacherDashboard = ({ games }) => {
    const router = useRouter()

    const [yourDrafts, setYourDrafts] = useState([])
    const [yourGames, setYourGames] = useState([])

    // const fetchDrafts = async () => {
    //     console.log("object");
    //     console.log('/api/game/teacher' + Cookies.get("id"));
    //     try {
    //         const res = await axios.get('/api/game/teacher/' + Cookies.get("id"), {
    //             headers: {
    //                 'authorization': `Bearer ${Cookies.accessToken}`
    //             }
    //         })

    //         console.log(res.data);


    //         setYourDrafts(res.data)
    //     } catch (e) {
    //         console.log(e)
    //     }

    // }

    // useEffect(() => {
    //     fetchDrafts()
    // }, [])




    return (
        <TeacherLayout page="/teacher">
            <div className="teacher-dashboard w-full h-full p-8 overflow-x-hidden overflow-y-scroll" id='customscroll'>
                <div className="dashboard-header flex justify-between items-center mb-10">
                    <div className="your-dashboard">
                        <h6 className='font-bold text-[1.2rem] sm:text-[1.5rem] text-primary-600'>Your Dashboard</h6>
                        <p className='text-[.8rem] sm:text-[.9rem] text-[#7B7B7B]'>View your games and drafts here</p>
                    </div>
                    <Link href="/teacher/profile">
                        <div className="profile-img hover:cursor-pointer hover:scale-105 transition-all sm:block hidden">
                            <img className='w-14 h-14 rounded-full object-cover' src="/assets/memoji-pf.png" alt="profile" />
                        </div>
                    </Link>
                </div>
                <div className="dashboard-content">
                    <div className="your-games mb-8">
                        <div className="your-games-header flex flex-col sm:flex-row items-start justify-between mb-4 sm:mb-0">
                            <div className="your-game-title mb-4">
                                <h6 className='font-bold text-[1rem] sm:text-[1.2rem]'>Your Previous Games</h6>
                                <p className='text-[.7rem] sm:text-[.8rem] text-[#7B7B7B]'>List of your games that was played before</p>
                            </div>
                            {
                                games.length !== 0
                                    ? <Link href="/teacher/games">
                                        <p className='text-[.9rem] bg-primary-600 sm:bg-white mb-2 text-white sm:text-black px-4 sm:px-0 py-2 sm:py-2 rounded-md hover:text-primary-600 hover:cursor-pointer hover:scale-100 hover:font-bold transition-all'>View All</p>
                                    </Link>
                                    : <></>
                            }
                        </div>
                        {
                            games.length !== 0
                                ? <div className="game-card-list grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-2">
                                    {
                                        games && games.map((game, index) => {
                                            return (
                                                <GameCard key={index} game={game} />
                                            )
                                        })
                                    }
                                </div>
                                : <div className="w-full relative flex flex-col items-center justify-center">
                                    <h6 className='font-medium text-primary-600 text-[.8rem] sm:text-[.9rem] lg:text-md my-8'>You have not started any games yet</h6>
                                </div>
                        }
                    </div>
                    {/* <div className="your-drafts">
                        <div className="your-drafts-header flex flex-col sm:flex-row items-start justify-between mb-4 sm:mb-0">
                            <div className="your-drafts-title mb-4">
                                <h6 className='font-bold text-[1rem] sm:text-[1.2rem]'>Your Drafts</h6>
                                <p className='text-[.7rem] sm:text-[.8rem] text-[#7B7B7B]'>List of your draft games</p>
                            </div>
                            {
                                yourGames.length !== 0
                                ? <Link href="/teacher/drafts">
                                    <p className='text-[.9rem] mb-4 sm:mb-2 bg-primary-600 sm:bg-white text-white sm:text-black px-4 sm:px-0 py-2 sm:py-2 rounded-md hover:text-primary-600 hover:cursor-pointer hover:scale-100 hover:font-bold transition-all'>View All</p>
                                </Link>
                                : <></>
                            }
                        </div>
                        {
                            yourDrafts.length !== 0
                            ? <div className="game-card-list grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-2">
                                {
                                    yourDrafts && yourDrafts.map((game, index) => {
                                        return (
                                            <GameCard key={index} isDraft={true} game={game} />
                                        )
                                    })
                                }
                            </div>
                            :  <div className="w-full relative flex flex-col items-center justify-center">
                                    <h6 className='font-medium text-primary-600 text-[.8rem] sm:text-[.9rem] lg:text-md my-8'>You have not created any games yet</h6>
                                </div>
                        }
                    </div> */}
                </div>
            </div>
        </TeacherLayout>
    )
}

export default TeacherDashboard

export async function getServerSideProps({ req, res }) {
    let cookie = getCookie(req.headers.cookie)

    const accessToken = cookie.accessToken
    if (!accessToken) {
        return {
            redirect: {
                destination: '/log-in',
                permanent: false
            },
        }
    }

    if (cookie.role !== "Teacher") {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }

    const response = await axios.get(`/api/game/teacher/${cookie.id}`, {
        headers: {
            'authorization': `Bearer ${cookie.accessToken}`
        }
    })

    return {
        props: {
            games: response.data
        }, // will be passed to the page component as props
    }


}