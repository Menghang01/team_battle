import React, { useState, Fragment, useEffect } from 'react'
import axios from 'axios'
import AnswerCard from '../../components/answer_card'
import QuizCard from '../../components/quiz_card'
import { Menu, Transition } from '@headlessui/react'
import Cookies from 'cookies-js'
import { Router, useRouter } from 'next/router'
import { getCookie } from '../../utils/helper'
const CreateGame = () => {
    const router = useRouter()

    // current user information
    const [userInfo, setUserInfo] = useState({ userName: "Menghang" })

    useEffect(() => {
        var retrievedUserInfo = localStorage.getItem('userInfo');
        if (retrievedUserInfo) {
            setUserInfo(JSON.parse(retrievedUserInfo))

        }
    }, [])


    // show edit quiz info modal (title, description)
    const [showEdit, setShowEdit] = useState(false)
    const toggleShowEdit = () => setShowEdit(!showEdit)

    // error handler for questions and answers
    const [quizTitleEmpty, setQuizTitleEmpty] = useState(false)
    const [quizDescriptionEmpty, setQuizDescriptionEmpty] = useState(false)
    const [questionListEmpty, setQuestionListEmpty] = useState(false)

    // quiz title and description variables
    const [quizTitle, setQuizTitle] = useState("")
    const [quizDescription, setQuizDescription] = useState("")

    const onQuizTitleChange = e => setQuizTitle(e.target.value)
    const onQuizDescriptionChange = e => setQuizDescription(e.target.value)

    // questions list variable
    const [questionList, setQuestionList] = useState([
        {
            question: ''
        }
    ])
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0)

    // add an empty object to display the new question on the panel
    const onAddQuestion = () => setQuestionList([...questionList, {
        question: ''
    }])

    // onSelect handler for when a question is selected
    const onQuestionSelect = (index) => {
        setCurrentQuestionIndex(index)

        const questionAtIndex = questionList[index]

        if (questionAtIndex.question.length !== 0) {
            console.log(questionAtIndex);
            setQuestionTitle(questionAtIndex.question)
            setTimeLimit(questionAtIndex.answerTime)
            setAnswerList(questionAtIndex.answerList)
        } else {
            setQuestionTitle("")
            setTimeLimit(timeLimitList[0])
            setAnswerList([
                {
                    isCorrect: false,
                    body: ''
                },
                {
                    isCorrect: false,
                    body: ''
                },
                {
                    isCorrect: false,
                    body: ''
                },
                {
                    isCorrect: false,
                    body: ''
                },
            ])
        }
    }

    // error handler for questions and answers
    const [questionEmpty, setQuestionEmpty] = useState(false)
    const [answerEmpty, setAnswerEmpty] = useState(false)
    const [noCorrectSelected, setNoCorrectSelected] = useState(false)

    // question and answer variables
    const timeLimitList = [5, 10, 15, 20, 30]
    const [questionTitle, setQuestionTitle] = useState("")
    const [timeLimit, setTimeLimit] = useState(timeLimitList[0])
    const [answerList, setAnswerList] = useState([
        {
            isCorrect: false,
            body: ''
        },
        {
            isCorrect: false,
            body: ''
        },
        {
            isCorrect: false,
            body: ''
        },
        {
            isCorrect: false,
            body: ''
        },
    ])

    const onQuestionTitleChange = (e) => setQuestionTitle(e.target.value)
    const onTimeLimitSelectChange = (e) => setTimeLimit(parseInt(e.target.value))

    // select the correct answer
    const onSelectCorrectAnswer = (index) => {
        // find if there's any answer that isCorrect
        const found = answerList.findIndex(el => el.isCorrect);
        let temp = [...answerList]

        if (found === -1) {
            temp[index].isCorrect = !temp[index].isCorrect
        } else {
            if (found === index) {
                temp[index].isCorrect = !temp[index].isCorrect
            } else {
                temp[found].isCorrect = !temp[found].isCorrect
                temp[index].isCorrect = !temp[index].isCorrect
            }
        }

        setAnswerList(temp)
    }

    // onChange handler for each answer inpuut
    const onEachAnswerChange = (input, index) => {

        let temp = [...answerList]
        temp[index].body = input

        setAnswerList(temp)
    }

    // save changes for one question
    const onQuestionSaved = () => {

        // load error if the question has not been filled 
        if (questionTitle.length === 0) {
            setQuestionEmpty(true)
            return
        } else {
            setQuestionEmpty(false)
        }

        // load error if answers have not been filled 
        const foundEmptyAnswer = answerList.some(el => el.body.length === 0);
        if (foundEmptyAnswer) {
            setAnswerEmpty(true)
            return
        } else {
            setAnswerEmpty(false)
        }

        // load error if no correct answer has been selected 
        const foundNoCorrect = answerList.some(el => el.isCorrect);
        if (!foundNoCorrect) {
            setNoCorrectSelected(true)
            return
        } else {
            setNoCorrectSelected(false)
        }

        let questionData = {
            questionType: "Quiz",
            question: questionTitle,
            answerList: answerList,
            answerTime: timeLimit,
            answerList: answerList,
            questionIndex: questionList.length
        }

        // overwrite the list if the list is empty or append to the last index if it's not
        if (questionList.length > 1) {

            // update the current index with the new inputs
            let temp = [...questionList]
            temp[currentQuestionIndex] = questionData
            console.log(temp[currentQuestionIndex]);

            setQuestionList(temp)

        } else {
            setQuestionList([questionData])
        }
    }

    const onQuizSaved = async () => {
        // load error if the quiz title has not been filled
        if (quizTitle.length === 0) {
            setQuizTitleEmpty(true)
            return
        } else {
            setQuizTitleEmpty(false)
        }

        // load error if the quiz description has not been filled
        if (quizDescription.length === 0) {
            setQuizDescriptionEmpty(true)
            return
        } else {
            setQuizDescriptionEmpty(false)
        }

        const found = questionList.some(el => el.question === "")
        if (found) {
            setQuestionListEmpty(true)
            return
        } else {
            setQuestionListEmpty(false)
        }

        let quizData = {
            name: quizTitle,
            description: quizDescription,
            creatorName: userInfo.userName,
            creatorId: userInfo._id,
            questionList: questionList
        }

        try {
            const res = await axios.post('/api/quiz', quizData, {
                headers: {
                    'authorization': `Bearer ${Cookies.get("accessToken")}`
                }
            })
            if (res.status == 201) {
                quizData = {
                    name: "",
                    description: "",
                    creatorName: userInfo.userName,
                    creatorId: userInfo._id,
                    questionList: []
                }

                setQuizTitleEmpty("")
                setQuizDescription("")
                setAnswerList([
                    {
                        isCorrect: false,
                        body: ''
                    },
                    {
                        isCorrect: false,
                        body: ''
                    },
                    {
                        isCorrect: false,
                        body: ''
                    },
                    {
                        isCorrect: false,
                        body: ''
                    },
                ])

                setQuestionList([
                    {
                        question: ''
                    }
                ])
                setQuestionTitle("")
                alert("Quiz Created !")
                router.push('/teacher')
            }
        } catch (err) {
            console.log(err);
        }
    }

    const onBackClick = () => {
        router.push('/teacher')
    }

    return (
        <div className='create-game select-none'>
            <div className="background w-screen h-screen bg-white relative lg:overflow-y-hidden">
                <div className="create-game-main w-full h-full">

                    {/* pop up modal for quiz title and description */}
                    <div className={`${showEdit ? "block" : "hidden"} h-full w-full bg-[#3f3f3f] backdrop-blur-lg opacity-70 absolute z-50 transition-all hover:cursor-pointer`} onClick={() => toggleShowEdit()}></div>
                    <div className={`${showEdit ? "block" : "hidden"} w-1/3 top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 p-5 rounded-lg bg-white absolute z-50`}>
                        <h6 className='font-bold text-primary-600 text-lg mb-5'>Edit your quiz information</h6>
                        <div className="quiz-title mb-5">
                            <h6 className='font-bold text-black mb-2 text-sm'>Quiz title</h6>
                            <input
                                type="text"
                                className='bg-[#F3F4F6] w-full py-3 px-6 rounded-lg focus:outline-none'
                                placeholder='Title 1'
                                onChange={onQuizTitleChange}
                                value={quizTitle}
                            />
                        </div>
                        <div className="quiz-description mb-5">
                            <h6 className='font-bold text-black mb-2 text-sm'>Quiz Description</h6>
                            <textarea
                                style={{ "resize": "none" }}
                                rows="10"
                                type="text"
                                className='bg-[#F3F4F6] w-full py-3 px-6 rounded-lg focus:outline-none'
                                placeholder='Title 1'
                                onChange={onQuizDescriptionChange}
                                value={quizDescription}
                            />
                        </div>
                        <div className="quiz-submit flex space-x-2">
                            <div
                                className="quiz-question bg-primary-400 w-36 h-12 rounded-md font-bold flex justify-center items-center text-white hover:cursor-pointer hover:bg-primary-600 transition-all"
                                onClick={() => toggleShowEdit(true)}
                            >
                                <p className='font-bold'>Save Changes</p>
                            </div>
                        </div>
                    </div>

                    {/* Back icon */}
                    <div
                        className="navbar bg-primary-400 h-[3rem] w-full flex items-center px-4 hover:cursor-pointer"
                        onClick={() => onBackClick()}
                    >
                        <ion-icon name="chevron-back-outline" id="chevron-back-outline"></ion-icon>
                        <h6 className='ml-1 font-medium text-white text-[1.rem]'>Back</h6>
                    </div>
                    <div className="create-game-container pt-5 px-5 bg-[#FAFAFA]">
                        <div className="title-bar flex flex-col xs:flex-row xs:justify-between items-start xs:items-center mb-5">
                            <h6
                                className='font-bold text-[1.5rem] lg:text-[2rem] hover:cursor-pointer'
                                onClick={() => toggleShowEdit()}
                            >
                                {quizTitle.length !== 0 ? quizTitle : "Quiz Title"}
                            </h6>
                            <div className="quiz-action-btns flex items-center mt-4 lg:mt-0">
                                <div
                                    className="quiz-question border border-primary-400 text-primary-400 mr-4 w-fit px-4 lg:px-6 h-12 rounded-md font-bold flex justify-center items-center hover:cursor-pointer hover:border-primary-600 hover:text-primary-600 hover:scale-105 transition-all"
                                    onClick={() => toggleShowEdit()}
                                >
                                    <p className='font-bold text-[.9rem] lg:text-[1rem]'>Edit information</p>
                                </div>
                                <div
                                    className="quiz-question bg-primary-400 w-fit px-4 lg:px-6 h-12 rounded-md font-bold flex justify-center items-center text-white hover:cursor-pointer hover:bg-primary-600 hover:scale-105 transition-all"
                                    onClick={() => onQuizSaved()}
                                >
                                    <p className='font-bold text-[.9rem] lg:text-[1rem]'>Save Quiz</p>
                                </div>
                            </div>
                        </div>
                        {
                            quizTitleEmpty || quizDescriptionEmpty
                                ? <p className='text-[#ffaeae] text-[.9rem] mb-4'>* Please fill in the quiz information</p>
                                : <></>
                        }
                        {
                            questionListEmpty && <p className='text-[#ffaeae] text-[.9rem] mb-4'>* Please fill all the questions</p>
                        }
                        <div className="inner-container flex flex-col lg:flex-row">

                            {/* Quiz question side panel */}
                            <div className="your-quiz w-full lg:w-1/6">
                                <p className='font-bold text-primary-600 mb-2'>Your Quiz</p>
                                <div className='list overflow-scroll w-full lg:h-[35rem] flex flex-row lg:flex-col' id='customscroll'>
                                    <div className="quiz-list mr-2 flex flex-row lg:flex-col">
                                        {
                                            questionList &&
                                            questionList.map((question, index) => {
                                                return (
                                                    <QuizCard
                                                        key={index}
                                                        index={index}
                                                        content={question.question}
                                                        onQuestionSelect={onQuestionSelect}
                                                    />
                                                )
                                            })
                                        }
                                    </div>
                                    <div
                                        className="quiz-card relative mr-2 min-w-[15rem] lg:min-w-0 bg-white h-32 lg:h-36 rounded-md border-[3px] box-border text-[#979797] border-grey-500 hover:cursor-pointer hover:border-primary-400 hover:text-[#777777] transition-all flex items-center justify-center"
                                        onClick={() => onAddQuestion()}
                                    >
                                        <p className='text-center text-[1.1rem] font-bold flex items-center space-x-2'><ion-icon name="add-circle-outline" id="add-question"></ion-icon><span>Add Question</span></p>
                                    </div>
                                </div>
                            </div>

                            {/* Question form (question title, time limit and answers) */}
                            <div className="create-quiz-form bg-white w-full lg:w-5/6 rounded-md p-4 ml-2">
                                <div className="flex flex-col lg:flex-row w-full">
                                    <div className="quiz-question mb-5 w-full lg:w-[100%]">
                                        <div className="flex justify-between items-center">
                                            <h6 className='font-bold text-primary-600 mb-6'>Quiz Question</h6>
                                            {
                                                questionEmpty && <p className='text-[#ffaeae] text-[.9rem] mb-6'>* Please fill in the question</p>
                                            }
                                        </div>
                                        <input
                                            type="text"
                                            className='bg-[#F3F4F6] w-full py-4 px-6 rounded-lg focus:outline-none font-medium'
                                            placeholder='Example: How many grams in one kilogram?'
                                            onChange={(e) => onQuestionTitleChange(e)}
                                            value={questionTitle}
                                        />
                                    </div>
                                    {/* <div className="quiz-setting mb-6 lg:mb-0 lg:ml-5">
                                        <h6 className='font-bold text-primary-600 mb-4'>Quiz Setting</h6>
                                        <div className="">
                                            <div className="time-limit flex items-center space-x-1 mr-3 mb-2">
                                                <ion-icon name="stopwatch-outline" id="time-limit"></ion-icon>
                                                <p className='font-medium text-sm'>Time limit</p>
                                            </div>
                                            <div className="time-limit-dropdown w-full">
                                                <div>
                                                    <select
                                                        className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium leading-5 text-gray-700 transition duration-150 ease-in-out bg-white border border-[#D9D9D9] rounded-md hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800"
                                                        aria-label="Default select example"
                                                        onChange={onTimeLimitSelectChange}
                                                        value={timeLimit}
                                                        defaultValue={timeLimitList[0]}
                                                    >
                                                        {
                                                            timeLimitList.map((time, idx) =>
                                                                <option value={time} key={idx}>{time} seconds</option>
                                                            )
                                                        }
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div> */}
                                </div>
                                <div className="quiz-answer mb-5">
                                    <div className="w-full flex items-center">
                                        <h6 className='font-bold text-primary-600 mb-4 mr-4'>Quiz Answer</h6>
                                        {
                                            answerEmpty && <p className='text-[#ffaeae] text-[.9rem] mb-4'>* Please fill in all the answers</p>
                                        }
                                        {
                                            noCorrectSelected && <p className='text-[#ffaeae] text-[.9rem] mb-4'>* Please select a correct answer</p>
                                        }
                                    </div>
                                    <div className="answer-list grid grid-cols-2 gap-4">
                                        {
                                            answerList.map((answer, index) => {
                                                return (
                                                    <AnswerCard
                                                        key={index}
                                                        answer={answer}
                                                        index={index}
                                                        onSelectCorrectAnswer={onSelectCorrectAnswer}
                                                        onEachAnswerChange={onEachAnswerChange}
                                                    />
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                                <div
                                    className="quiz-question mb-5 bg-primary-400 w-36 h-12 rounded-md font-bold flex justify-center items-center text-white hover:cursor-pointer hover:bg-primary-600"
                                    onClick={() => onQuestionSaved()}
                                >
                                    <p className='font-bold'>Save Changes</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateGame


export async function getServerSideProps({ req, res }) {
    let cookie = getCookie(req.headers.cookie)

    const accessToken = cookie.accessToken
    if (!accessToken) {
        return {
            redirect: {
                destination: '/log-in',
                permanent: false
            },
        }
    }

    if (cookie.role !== "Teacher") {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }



    return {
        props: {

        }
    }


}