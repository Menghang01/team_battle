import React, { useEffect, useState } from 'react'
import TeacherLayout from '../../layout/teacher-layout'
import { getCookie } from '../../utils/helper'

const TeacherProfile = () => {
    // current user information
    const [userInfo, setUserInfo] = useState([])

    useEffect(() => {
        var retrievedUserInfo = localStorage.getItem('userInfo');
        setUserInfo(JSON.parse(retrievedUserInfo))
    }, [])

    const [showEditProfile, setShowEditProfile] = useState(false)

    const onToggleEditProfile = () => setShowEditProfile(!showEditProfile)

    return (
        <TeacherLayout>
            <div className="teacher-profile w-full p-5">
                <div className="profile-header h-60 relative">
                    <img className='cover relative w-full h-60 object-cover rounded-md' src="/assets/profile-cover.jpg" alt="" />
                    <div className="profile-group absolute -bottom-1/2 left-16">
                        <div className="sm:w-44 sm:h-44 lg:w-56 lg:h-56 bg-[#FEEBAE] border-4 border-white rounded-full flex justify-center items-center">
                            <img className='s:w-[8rem] sm:h-[8rem] lg:w-[10rem] lg:h-[10rem] object-cover rounded-full' src="/assets/default-pf.png" alt="pf" />
                        </div>
                    </div>
                    <div className='profile-name absolute sm:left-[16rem] lg:left-[19rem] sm:-bottom-[4.5rem] lg:-bottom-[5.5rem]'>
                        <h6 className='font-bold sm:text-[1.5rem] lg:text-[2rem]'>{`${userInfo.firstName} ${userInfo.lastName}`}</h6>
                        <p className='text-[#7A7A7A]'>Game Master</p>
                    </div>
                    {/* <div className='profile-name absolute text=[.8rem] right-0'>
                        <p className='text-primary-600 font-medium mt-4 hover:cursor-pointer hover:underline' onClick={() => onToggleEditProfile()}>Edit Profile</p>
                    </div> */}
                </div>
                {
                    false &&
                    <div className="statistics mt-32 border border-[#e9e9e9] rounded-md p-5">
                        <h6 className='font-bold text-[1.1rem]'>Your Statistics</h6>
                        <hr className='text-[#e9e9e9] my-4' />
                        <div className="stat-list flex">
                            {/* <div className="stat-item flex justify-start items-start mr-12">
                                <div className="w-8 h-8 bg-green-600 rounded-full mr-4 mt-2"></div>
                                <div className="stat-disc">
                                    <h6 className='p-0 m-0 font-bold text-[2rem]'>169</h6>
                                    <p>Total Students</p>
                                </div>
                            </div> */}
                            <div className="stat-item flex justify-start items-start mr-12">
                                <div className="w-8 h-8 bg-blue-600 rounded-full mr-4 mt-2"></div>
                                <div className="stat-disc">
                                    <h6 className='p-0 m-0 font-bold text-[2rem]'>14</h6>
                                    <p>Total Players</p>
                                </div>
                            </div>
                            <div className="stat-item flex justify-start items-start mr-12">
                                <div className="w-8 h-8 bg-orange-600 rounded-full mr-4 mt-2"></div>
                                <div className="stat-disc">
                                    <h6 className='p-0 m-0 font-bold text-[2rem]'>2</h6>
                                    <p>Total Drafts</p>
                                </div>
                            </div>
                            <div className="stat-item flex justify-start items-start mr-12">
                                <div className="w-8 h-8 bg-yellow-600 rounded-full mr-4 mt-2"></div>
                                <div className="stat-disc">
                                    <h6 className='p-0 m-0 font-bold text-[2rem]'>4</h6>
                                    <p>Total Games Played</p>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {
                    false &&
                    <div className="edit-profile mt-32 border border-[#e9e9e9] rounded-md p-5">
                        <h6 className='font-bold text-[1.1rem]'>Edit Your Profile Here</h6>
                        <hr className='text-[#e9e9e9] my-4' />
                        <div className="form flex">
                            <div className="name-input flex flex-col mb-6 mr-6">
                                <label className='text-[.9rem] font-bold mb-4'>First Name</label>
                                <input type="text" className='w-64 border border-[#808080] rounded-lg py-3 px-4 text-[.9rem] focus:outline-none' placeholder='Andrew' />
                            </div>
                            <div className="name-input flex flex-col mb-6">
                                <label className='text-[.9rem] font-bold mb-4'>Last Name</label>
                                <input type="text" className='w-64 border border-[#808080] rounded-lg py-3 px-4 text-[.9rem] focus:outline-none' placeholder='Kim' />
                            </div>
                        </div>
                        <div className="action-buttons flex">
                            <div className="bg-primary-400 mr-4 w-32 px-4 py-3 rounded-md font-medium text-[.9rem] flex justify-center items-center hover:cursor-pointer hover:bg-primary-600 transition-all">
                                <p className='text-white'>Save Changes</p>
                            </div>
                            <div className="bg-white border border-primary-600 text-primary-600 w-24 px-4 py-3 rounded-md font-medium text-[.9rem] flex justify-center items-center hover:cursor-pointer hover:bg-primary-600 hover:text-white transition-all" onClick={() => onToggleEditProfile()}>
                                <p>Cancel</p>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </TeacherLayout>
    )
}

export default TeacherProfile

export async function getServerSideProps({ req, res }) {
    let cookie = getCookie(req.headers.cookie)

    const accessToken = cookie.accessToken
    if (!accessToken) {
        return {
            redirect: {
                destination: '/log-in',
                permanent: false
            },
        }
    }

    if (cookie.role !== "Teacher") {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }



    return {
        props: {

        }
    }


}