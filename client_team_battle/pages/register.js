import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import axios from 'axios'
import Router from 'next/router'
import Cookies from 'cookies-js'
const Register = () => {
    const [showPassword, setShowPassword] = useState(false)
    const [showConfirmPassword, setShowConfirmPassword] = useState(false)

    const onShowPassword = () => setShowPassword(!showPassword)
    const onShowConfirmPassword = () => setShowConfirmPassword(!showConfirmPassword)

    const [role, setRole] = useState("")
    const onRoleChange = input => setRole(input)

    // form validation

    const rules = Yup.object().shape({
        firstName: Yup.string()
            .required('First Name is required'),
        lastName: Yup.string()
            .required('Last Name is required'),
        mail: Yup.string()
            .required('Email is required')
            .email('Email is invalid'),
        password: Yup.string()
            .min(4, 'Password must be at least 6 characters')
            .required('Password is required'),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Passwords must match')
            .required('Confirm Password is required'),
    })

    const formOptions = { resolver: yupResolver(rules) };
    const { register, handleSubmit, formState: { errors } } = useForm(formOptions)

    const onSubmit = async (data) => {

        data.userType = role
        data.userName = data.firstName + data.lastName
        console.log(data);

        const res = await axios.post('/api/user', data)

        if (res.status == 201) {
            localStorage.setItem('userInfo', JSON.stringify(res.data.result))

            Cookies.set("accessToken", res.data.accessToken)
            Cookies.set("role", res.data.result.userType)
            Cookies.set("id", res.data.result._id)

            if (res.data.userType === "Teacher") {
                Router.replace("/teacher")

            } else {
                Router.replace("/join-quiz")

            }

        }

    }

    return (
        <div className="main">
            <div className="background w-screen h-screen bg-primary-600 relative overflow-hidden">
                <img className='w-3/5 h-3/5 absolute -top-5 -left-64' src="/assets/ring.svg" alt="doodle" />
                <img className='w-3/5 h-3/5 absolute -bottom-5 -right-64' src="/assets/hexa.svg" alt="doodle" />
                <div className="login-form w-screen h-screen absolute flex items-center justify-center">
                    <div className="form-container w-[22rem] lg:w-[30rem] bg-white rounded-lg flex flex-col items-center justify-between py-5 px-8 lg:px-12">
                        <div className="login-head w-full">
                            <div className="w-full flex flex-col justify-center items-center border-b border-[#80808050]">
                                <h6 className='font-bold text-[1.1rem] lg:text-[1.4rem] mb-1'>Welcome to Team Battle!</h6>
                                <p className='text-[.8rem] text-[#808080] mb-4'>Create a new account here</p>
                            </div>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className='flex flex-col justify-center items-center'>
                                    <div className="form w-full flex flex-col items-center pt-4">
                                        <div className="name-input flex w-full items-center">
                                            <div className="flex flex-col w-full mb-4 mr-2">
                                                <div className="flex items-center mb-1">
                                                    <label className='text-[.9rem] font-bold mr-3'>First Name</label>
                                                    {errors.firstName && <p className='text-[#ffaeae] text-[.9rem]'>* Required</p>}
                                                </div>
                                                <input type="text" {...register("firstName")} className={`w-full rounded-lg py-3 px-4 text-[.9rem] focus:outline-none ${errors.firstName ? "border-2 border-[#ffaeae]" : "border border-[#808080]"}`} placeholder='Ex: John' />
                                            </div>
                                            <div className="flex flex-col w-full mb-4 ml-2">
                                                <div className="flex items-center mb-1">
                                                    <label className='text-[.9rem] font-bold mr-3'>Last Name</label>
                                                    {errors.lastName && <p className='text-[#ffaeae] text-[.9rem]'>* Required</p>}
                                                </div>
                                                <input type="text" {...register("lastName")} className={`w-full rounded-lg py-3 px-4 text-[.9rem] focus:outline-none ${errors.lastName ? "border-2 border-[#ffaeae]" : "border border-[#808080]"}`} placeholder='Ex: Doe' />
                                            </div>
                                        </div>
                                        <div className="email-input flex flex-col w-full mb-4">
                                            <div className="flex items-center mb-1">
                                                <label className='text-[.9rem] font-bold mb-1 mr-3'>Email</label>
                                                {errors.mail && <p className='text-[#ffaeae] text-[.9rem]'>* Required</p>}
                                            </div>
                                            <input type="text" {...register("mail")} className={`w-full border border-[#808080] rounded-lg py-3 px-4 text-[.9rem] focus:outline-none ${errors.mail ? "border-2 border-[#ffaeae]" : "border border-[#808080]"}`} placeholder='Ex: john.doe@gmail.com' />
                                        </div>
                                        <div className="password-input flex flex-col w-full mb-4">
                                            <div className="flex items-center mb-1">
                                                <label className='text-[.9rem] font-bold mb-1 mr-3'>Password</label>
                                                {errors.password && <p className='text-[#ffaeae] text-[.9rem]'>* Required</p>}
                                            </div>
                                            <div className={`pw-input w-full rounded-lg py-3 px-4 text flex justify-between items-center ${errors.password ? "border-2 border-[#ffaeae]" : "border border-[#808080]"}`}>
                                                <input
                                                    type={showPassword ? "text" : "password"}
                                                    className='w-5/6 text-[.9rem] focus:outline-none'
                                                    placeholder='Enter your password here'
                                                    {...register("password")}
                                                />
                                                {showPassword ? <ion-icon name="eye" className="cursor-pointer" id="check-pw" onClick={() => onShowPassword()}></ion-icon> : <ion-icon name="eye-outline" className="cursor-pointer" id="check-pw" onClick={() => onShowPassword()}></ion-icon>}
                                            </div>
                                        </div>
                                        <div className="password-input flex flex-col w-full mb-4">
                                            <div className="flex items-center mb-1">
                                                <label className='text-[.9rem] font-bold mb-1 mr-3'>Confirm Password</label>
                                                {errors.confirmPassword && <p className='text-[#ffaeae] mb-1 text-[.9rem]'>{errors.confirmPassword.message}</p>}
                                            </div>
                                            <div className={`pw-input w-full rounded-lg py-3 px-4 text flex justify-between items-center ${errors.password ? "border-2 border-[#ffaeae]" : "border border-[#808080]"}`}>
                                                <input
                                                    type={showConfirmPassword ? "text" : "password"}
                                                    className='w-5/6 text-[.9rem] focus:outline-none'
                                                    placeholder='Re-enter your password here'
                                                    {...register("confirmPassword")}
                                                />
                                                {showConfirmPassword ? <ion-icon name="eye" className="cursor-pointer" id="check-pw" onClick={() => onShowConfirmPassword()}></ion-icon> : <ion-icon name="eye-outline" className="cursor-pointer" id="check-pw" onClick={() => onShowConfirmPassword()}></ion-icon>}
                                            </div>
                                        </div>
                                        <div className="flex flex-col w-full">
                                            <div className="flex items-center mb-1">
                                                <label className='text-[.9rem] font-bold mb-1 mr-3'>Select your role</label>
                                                {errors.userType && <p className='text-[#ffaeae] text-[.9rem]'>* Required</p>}
                                            </div>
                                            <div className="radio flex">
                                                <div className="flex hover:cursor-pointer mr-5">
                                                    <input type="radio" onChange={(e) => onRoleChange(e.target.value)} value="Teacher" name="select_role" id="teacher" className='mr-2' />
                                                    <label for="teacher" className='text-[.9rem] font-medium hover:cursor-pointer'>I am a teacher</label>
                                                </div>
                                                <div className="flex hover:cursor-pointer">
                                                    <input type="radio" onChange={(e) => onRoleChange(e.target.value)} value="Student" name="select_role" id="student" className='mr-2' />
                                                    <label for="student" className='text-[.9rem] font-medium hover:cursor-pointer'>I am a student</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="submit bg-primary-400 w-fit px-6 py-2 mt-6 rounded-md hover:box-border text-white font-medium lg:font-bold transition-all hover:cursor-pointer hover:bg-primary-600">
                                        <input type="submit" value="Register" className='hover:cursor-pointer' />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Register


export async function getServerSideProps({ req, res }) {
    const accessToken = req.headers.cookie
    if (accessToken) {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }
    return {
        props: {}, // will be passed to the page component as props
    }


}
