import React from 'react';
import { useSelector } from 'react-redux';

const Test = () => {
    const socket = useSelector(state => state.socket.socket)
    let index = 0
    
    const next = () => {
        socket.emit("next-question", index)
        index += 1
    }
    return (
        <div>
            <button onClick={next}>test </button>
        </div>
    );
}

export default Test;
