import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import { useDispatch, useSelector } from "react-redux"
import axios from "axios"
import Cookies from "cookies-js"

import Router from 'next/router'
import { createGame, storeGame } from '../action/gameAction'

const TeamSetting = () => {
    useEffect(() => {
        if (game === null) {
            Router.push('/teacher/games')
        }
    }, [])

    const teamCountList = [1, 2, 3, 4, 5, 6, 7, 8]
    const [teamCount, setTeamCount] = useState(teamCountList[0])
    const onTeamCountSelectChange = (e) => setTeamCount(e.target.value)

    const [teamAllocationError, setTeamAllocationError] = useState(false)
    const [teamAllocation, setTeamAllocation] = useState("")
    const onTeamAllocationChange = (e) => setTeamAllocation(e.target.value)

    const onNextClick = () => {
        if (teamAllocation.length === 0) {
            setTeamAllocationError(true)
            return
        } else {
            setTeamAllocationError(false)
        }

        const body = {
            teamCount: teamCount,
            teamAllocation: teamAllocation
        }

        console.log(body);
    }

    const dispatch = useDispatch()
    const game = useSelector(state => state.game.game)
    const socket = useSelector(state => state.socket.socket)
    const setup_team = (e) => {
        console.log(e.target.name, e.target.id)
        dispatch(createGame({
            ...game,
            [e.target.name]: e.target.id
        }))

    }

    const submitGame = async () => {

        if (game['allocation_setting'] === undefined) {
            setTeamAllocationError(true)
            return
        }

        try {
            console.log({
                ...game,
                team_count: teamCount

            });
            const response = await axios.post("/api/game", {
                ...game,
                team_count: parseInt(teamCount)

            }, {
                headers: {
                    'authorization': `Bearer ${Cookies.get("accessToken")}`
                }
            })


            if (response.status === 201) {
                socket.emit("init-game", response.data, (data) => {
                    console.log("init-game")
                    console.log(data);
                    dispatch(storeGame(data))
                    
                    if (data["allocation_setting"] === "manual") {
                        Router.push("/team-selection")
                    } else {
                        Router.push("/team-selection-auto")
                    }
                })

                // Router.push("/team-selection")


            }

        } catch (e) {
            console.log(e)
        }
    }
    return (
        <div className="main">
            <div className="background w-screen h-screen bg-primary-600 relative overflow-hidden">
                <img className='w-4/5 h-4/5 absolute -bottom-64 -left-96 z-1' src="/assets/radio_ring.svg" alt="doodle" />
                <h6 className='absolute pl-5 pt-5 font-bold text-white text-[1.5rem]'>LOGO</h6>
                <div className="login-form w-screen h-full absolute flex flex-col items-center justify-center">
                    <div className="form-container w-68 sm:w-96 bg-white rounded-lg flex flex-col items-center justify-between py-5 px-7">
                        <div className="login-head w-full">
                            <div className="flex flex-col items-center justify-center w-full pb-5 border-b border-[#E0E0E0]">
                                <h6 className='font-bold text-[1.2rem] sm:text-[1.6rem] mb-1'>Team Setting</h6>
                            </div>
                            <div className="form w-full flex flex-col items-center pt-4">
                                <div className="flex flex-col w-full mb-6">
                                    <label className='text-[.75rem] sm:text-[.9rem] font-bold mb-3'>Select number of team (Maximum of 8)</label>
                                    <div className="pw-input w-full">
                                        <div className="member-dropdown">
                                            <select
                                                className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium leading-5 text-gray-700 transition duration-150 ease-in-out bg-white border border-[#D9D9D9] rounded-md hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800"
                                                aria-label="Default select example"
                                                onChange={onTeamCountSelectChange}
                                                value={teamCount}
                                                defaultValue={teamCountList[0]}
                                            >
                                                {
                                                    teamCountList.map((time, idx) =>
                                                        <option value={time} key={idx}>{time} teams</option>
                                                    )
                                                }
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex flex-col w-full">
                                    <div className="flex items-center mb-3">
                                        <label className='text-[.75rem] sm:text-[.9rem] font-bold mr-4'>Team allocation</label>
                                        {teamAllocationError && <p className='text-[#ffaeae] text-[.9rem]'>* Required</p>}
                                    </div>
                                    <div className="radio flex">
                                        <div className="radio flex">
                                            <div className="flex hover:cursor-pointer mr-5">
                                                <input
                                                    type="radio"
                                                    value="manual"
                                                    onChange={setup_team}
                                                    name="allocation_setting"
                                                    id="manual"
                                                    className='mr-2'
                                                />
                                                <label for="manual" className='text-[.75rem] sm:text-[.9rem] font-medium'>Manually</label>
                                            </div>
                                            <div className="flex hover:cursor-pointer">
                                                <input
                                                    type="radio"
                                                    value="auto"
                                                    onChange={setup_team}
                                                    name="allocation_setting"
                                                    id="auto"
                                                    className='mr-2'
                                                />
                                                <label for="auto" className='text-[.75rem] sm:text-[.9rem] font-medium'>Automatically</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex justify-center">
                                        <div onClick={submitGame} className="submit bg-primary-400 text-[.75rem] sm:text-[.9rem] mt-8 px-5 py-2 rounded-md hover:box-border text-white transition-all hover:cursor-pointer hover:bg-primary-600">
                                            <p className='font-bold'>Start Game</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TeamSetting