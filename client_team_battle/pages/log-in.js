import Link from 'next/link'
import React, { useState } from 'react'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useRouter } from 'next/router'
import Cookies from 'cookies-js'
const Login = () => {
    const router = useRouter()

    const [showPassword, setShowPassword] = useState(false)
    const onShowPassword = () => setShowPassword(!showPassword)

    const rules = Yup.object().shape({
        mail: Yup.string()
            .required('Email is required')
            .email('Email is invalid'),
        password: Yup.string()
            .min(6, 'Password must be at least 6 characters')
            .required('Password is required'),
    })

    const formOptions = { resolver: yupResolver(rules) };
    const { register, handleSubmit, formState: { errors } } = useForm(formOptions)

    const [loginError, setLoginError] = useState(false)

    const onSubmit = async data => {

        console.log(data);

        try {
            const res = await axios.post('/api/login', data)

            if (res.data === "Not allowed") {
                setLoginError(true)
            } else {
                localStorage.setItem('userInfo', JSON.stringify(res.data.result))
                Cookies.set("accessToken", res.data.accessToken)
                Cookies.set("role", res.data.result.userType)
                Cookies.set("id", res.data.result._id)

                if (res.data.result.userType !== 'Teacher') {
                    router.push('/join-quiz')
                } else {
                    router.push('/teacher')
                }
            }

        } catch (err) {
            console.log(err)
            setLoginError(true)
        }

    }

    return (
        <div className="main">
            <div className="background w-screen h-screen bg-primary-600 relative overflow-hidden">
                <img className='w-3/5 h-3/5 absolute -top-5 -left-64' src="/assets/ring.svg" alt="doodle" />
                <img className='w-3/5 h-3/5 absolute -bottom-5 -right-64' src="/assets/hexa.svg" alt="doodle" />
                <div className="login-form w-screen h-screen absolute flex items-center justify-center">
                    <div className="form-container w-[22rem] lg:w-[30rem] bg-white rounded-lg flex flex-col items-center justify-between py-5 px-8 lg:px-12">
                        <div className="login-head w-full">
                            <div className="flex flex-col items-center w-full pb-5 border-b border-[#E0E0E0]">
                                <h6 className='font-bold text-[1.1rem] lg:text-[1.4rem] mb-1'>Welcome!</h6>
                                <p className='text-[.8rem] text-[#808080]'>New here? <span className='text-primary-600 hover:underline'><Link href="/register">Create a new account</Link></span></p>
                            </div>
                            <form onSubmit={handleSubmit(onSubmit)}>
                                <div className="flex flex-col justify-center items-center">
                                    <div className="form w-full flex flex-col justify-center items-center pt-4">
                                        {
                                            loginError &&
                                            <p className='text-[#ffaeae] text-sm mb-2'>The email or password you entered is incorrect.</p>
                                        }
                                        <div className="email-input flex flex-col w-full mb-6">
                                            <div className="flex items-center mb-1">
                                                <label className='text-[.9rem] font-bold mr-3'>Email</label>
                                                {errors.email && <p className='text-[#ffaeae] text-[.9rem]'>* Required</p>}
                                            </div>
                                            <input
                                                type="text"
                                                className={`w-full rounded-lg py-3 px-4 text-[.9rem] focus:outline-none ${errors.firstName ? "border-2 border-[#ffaeae]" : "border border-[#808080]"}`}
                                                placeholder='Ex: john.doe@gmail.com'
                                                {...register("mail")}
                                            />
                                        </div>
                                        <div className="password-input flex flex-col w-full">
                                            <div className="flex items-center mb-1">
                                                <label className='text-[.9rem] font-bold mr-3'>Password</label>
                                                {errors.password && <p className='text-[#ffaeae] text-[.9rem]'>* Required</p>}
                                            </div>
                                            <div className={`pw-input w-full rounded-lg py-3 px-4 text flex justify-between items-center ${errors.password ? "border-2 border-[#ffaeae]" : "border border-[#808080]"}`}>
                                                <input
                                                    type={showPassword ? "text" : "password"}
                                                    className='w-5/6 text-[.9rem] focus:outline-none'
                                                    placeholder='Enter your password here'
                                                    {...register("password")}
                                                />
                                                {
                                                    showPassword
                                                        ? <ion-icon name="eye" id="check-pw" onClick={() => onShowPassword()}></ion-icon>
                                                        : <ion-icon name="eye-outline" id="check-pw" onClick={() => onShowPassword()}></ion-icon>
                                                }

                                            </div>
                                        </div>
                                    </div>
                                    <div className="submit bg-primary-400 w-fit px-6 py-2 mt-8 rounded-md hover:box-border text-white font-medium lg:font-bold transition-all hover:cursor-pointer hover:bg-primary-600">
                                        <input type="submit" value="Log in" className='hover:cursor-pointer' />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login


export async function getServerSideProps({ req, res }) {
    const accessToken = req.headers.cookie
    if (accessToken) {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }
    return {
        props: {}, // will be passed to the page component as props
    }
}
