import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux';
import { getCookie } from '../utils/helper';
import axios from "axios"
import { useRouter } from 'next/router';
import TeamSetting from './team-setting';
import SandLoading from '../components/lottie/loading';

const PlayGame = ({ game, isSelected = false, isCorrect = false, isTeacher = true }) => {
    const router = useRouter()
    const [index, setIndex] = useState(0)
    const [teamIndex, setTeamIndex] = useState(0)
    const socket = useSelector(state => state.socket.socket)
    const current_game_state = useSelector((state) => state.game.current_game)
    const [isLast, setIsLast] = useState(false)

    const [team, setTeam] = useState(router.query.team)
    const [teamList, setTeamList] = useState([])
    const [teamListScore, setTeamListScore] = useState([])

    const [currentGame, setCurrentGame] = useState(game)

    const [showBoss, setShowBoss] = useState(true)
    const [bossDead, setBossDead] = useState(false)
    const [teamDead, setTeamDead] = useState(false)

    let team_index;


    useEffect(() => {
        if (typeof window !== "undefined") {

            localStorage.setItem("index", 0)

            // const unloadCallback = (event) => {
            //     event.preventDefault();
            //     event.returnValue = "";
            //     return "";
            // };

            // window.addEventListener("beforeunload", unloadCallback);
            // return () => window.removeEventListener("beforeunload", unloadCallback);

        }

    }, []);

    // heatlh bar initializer and functions
    const [bossTotalHealth, setBossTotalHealth] = useState(currentGame.boss_health)
    const [bossRemainingHealth, setBossRemainingHealth] = useState(currentGame.boss_health)
    const [bossRemainingHealthPercentage, setBossRemainingHealthPercentage] = useState(100)

    const [teamTotalHealth, setTeamTotalHealth] = useState(currentGame.base_team_health)
    const [teamRemainingHealth, setTeamRemainingHealth] = useState(currentGame.teamStatistic)

    const [teamScore, setTeamScore] = useState([])
    const [teamDamageList, setTeamDamageList] = useState({})
    const [damage, setDamage] = useState([])
    const [sumPerRound, setSumPerRound] = useState(0)

    const [teamDamage, setTeamDamage] = useState(0)

    const [showLoading, setShowLoading] = useState(false)

    useEffect(() => {
        console.log(router.query)
        console.log("CURRENT GAME: ");
        console.log(currentGame);

        setBossTotalHealth(currentGame.boss_health)
        setBossRemainingHealth(currentGame.boss_health)

        setTeamRemainingHealth(currentGame.teamStatistic)
        setTeamTotalHealth(currentGame.base_team_health)

        console.log(teamRemainingHealth)

    }, [currentGame]);

    useEffect(() => {
        if (socket) {

            socket.on("end-game-stats", (state) => {
                router.push({
                    pathname: '/leaderboad',
                    query: {
                        stats: JSON.stringify(state)
                    }
                }, 'leaderboard')
            })

            socket.on("team-died", (tName) => {
                console.log(tName, team)
                if (tName === team) {
                    router.replace({
                        pathname: '/team-died',

                    }, 'team-died')

                }

            })

            socket.on("team-all-dead", (state) => {
                localStorage.setItem("team_stats", JSON.stringify(state))

                setTimeout(() => {
                    localStorage.setItem("team_stats", JSON.stringify(state))

                }, 300);

                router.push({
                    pathname: '/leaderboard',
                    query: {
                        stats: JSON.stringify(state)
                    }

                }, 'leaderboard')



            })


            socket.on("attack", ({ canHit, status, team_score }) => {

                console.log(team_score);

                let qIndex = localStorage.getItem('index')

                let tempTeamDamageList = teamDamageList
                tempTeamDamageList[team_score.team_name] = team_score.answer[qIndex]
                setTeamDamageList(tempTeamDamageList);
                console.log(tempTeamDamageList);

                setDamage(prev => [...prev, team_score.answer[qIndex].damagedInflicted]);
                setTeamScore(prev => [...prev, team_score])
                setTeamListScore(prev => [...prev, team_score.totalDamageInflicted])
            })


            socket.on("next-question-screen", (index) => {
                setIndex(index)
                let idx = parseInt(localStorage.getItem("index"))
                localStorage.setItem("index", idx += 1)
            })

            socket.on("game-stat", (state) => {
                localStorage.setItem("team_stats", JSON.stringify(state))

                setTimeout(() => {
                    localStorage.setItem("team_stats", JSON.stringify(state))

                }, 300);


                router.push({
                    pathname: '/leaderboad',
                    query: {
                        stats: JSON.stringify(state)
                    }
                }, '/leaderboard')


            })


            socket.emit("game-joined", (game, team_name) => {
                setCurrentGame(game)

                const tempList = []

                Object.keys(team_name).map((tname, index) => {
                    const temp = {
                        name: tname,
                        score: 0,
                        showFade: false,
                    }

                    console.log(temp);
                    tempList.push(temp)
                })

                if (tempList.length !== 0) {
                    setTeamList(tempList)
                }
            })

            // return () => {
            //     socket.disconnect()
            // }

        }


    }, [socket]);

    // countdown initializer and functions
    const [initialTime, setInitialTime] = useState(0);
    const [startTimer, setStartTimer] = useState(false);

    const nextQuestion = () => {
        socket.emit("next-question", (isLastQuestion) => {
            setIsLast(isLastQuestion)
        })
        console.log(isLast);

    }

    useEffect(() => {
        clearTimeout()
        setInitialTime(10)
        setStartTimer(true)
        setShowLoading(false)
        localStorage.setItem('gameIndex', 0)

    }, [index])

    useEffect(() => {

        let timer
        if (initialTime > 0 && startTimer !== false) {
            timer = setTimeout(() => {
                setInitialTime(initialTime - 1);
            }, 1000);
        }

        if (initialTime === 0 && startTimer) {
            setStartTimer(false);
            let sum;

            console.log(damage)

            let teamDieCounter = 0

            teamScore.map((team, index) => {
                console.log(team.team_name)
                if (team.team_health === null || team.team_health < 0) {
                    console.log("null team health");

                    teamDieCounter += 1
                    console.log('teamDieCounter', teamDieCounter);
                    if (team.team_name === router.query.team) {
                        router.replace("/team-died")
                    }
                }
            })

            if (teamDieCounter === teamList.length) {
                onAllTeamDie()
            }

            if (damage.length != 0) {
                let found = damage.findIndex((el => el < 0))
                if (found > -1) {
                    let temp = [...damage]
                    temp.splice(found, 1)
                    setDamage(temp)
                    sum = temp[0]

                    console.log(temp)
                    console.log(sum);
                } else {
                    sum = damage.reduce((accuulator, curr) => accuulator + curr)
                }
                setSumPerRound(sum)
            }

            let tempTeamScore = [...teamScore]
            let tempRemainingHealth = teamRemainingHealth

            tempTeamScore.map((team, index) => {
                Object.keys(teamRemainingHealth).map((teamName) => {
                    if (teamName === team.team_name) {
                        tempRemainingHealth[teamName]['team_health'] = tempTeamScore[index].team_health
                    }
                })
            })

            setTeamRemainingHealth(tempRemainingHealth)
            setTeamScore([])

            let tempTeamList = [...teamList]
            tempTeamList.map((team, index) => team.score = teamListScore[index])
            setTeamList(tempTeamList)

            onShowFadeBoss()

            console.log(bossRemainingHealth);

            if (sum > 0) {
                if (bossRemainingHealth - sum <= 0) {
                    setBossRemainingHealth(0)
                    onBossDie()

                } else {
                    setBossRemainingHealth(prev => prev - sum)
                }
            } else if (sum === undefined || sum === null) {
                setBossRemainingHealth(prev => prev)
            }

            setDamage([])
            setTeamListScore([])


        }
        console.log(initialTime, isLast);

        if (initialTime === 0 && isLast) {
            console.log("objects");
            if (bossDead) {
                setTimeout(() => {
                    socket.emit("time-up", (teamStatistic) => {
                        router.push({
                            pathname: "/leaderboard",
                            query: teamStatistic
                        }, "/leaderboard")
                    })
                }, 6000);
            } else {
                setTimeout(() => {
                    socket.emit("time-up", (teamStatistic) => {
                        router.push({
                            pathname: "/leaderboard",
                            query: teamStatistic
                        }, "/leaderboard")
                    })
                }, 2500);
            }

        }
        return () => clearTimeout(timer);
    }, [initialTime, startTimer])


    const sendAnswer = (value, isCorrect) => {
        let data = {
            team: team,
            userInfo: JSON.parse(localStorage.getItem("userInfo")),
            answer: value,
            time_taken: 10 - initialTime,
            isCorrect: isCorrect

        }

        setShowLoading(!showLoading)
        socket.emit("answer-question", data)
    }

    const [showFadeBoss, setShowFadeBoss] = useState(false)

    const onShowFadeBoss = () => {
        setShowFadeBoss(true)
        setTimeout(() => {
            setShowFadeBoss(false)
        }, 3000);
    }

    const onBossDie = () => {
        setBossDead(true)
        setTimeout(() => {
            setShowBoss(false)
        }, 3000);
        setTimeout(() => {
            socket.emit("time-up", (teamStatistic) => {
                router.push({
                    pathname: "/leaderboard",
                    query: teamStatistic
                }, "/leaderboard")
            })
        }, 7000);
    }

    const onAllTeamDie = () => {
        setTeamDead(true)
        setTimeout(() => {
            socket.emit("time-up", (teamStatistic) => {
                router.push({
                    pathname: "/leaderboard",
                    query: teamStatistic
                }, "/leaderboard")
            })
        }, 6000);
    }

    return (
        <div className="play-game w-screen h-screen py-3 px-5 select-none overflow-x-hidden" id='customscroll'>
            <h6
                className='font-bold text-primary-600 text-[1.8rem] mb-4'
            >LOGO</h6>
            <div className="boss-container w-full h-[30rem] xs:h-[20rem] sm:h-72 relative">
                <img className='absolute w-full h-[30rem] xs:h-[20rem] sm:h-72 rounded-lg object-cover' src="/assets/quiz-background.png" alt="quiz-background" />
                <div className="absolute w-full h-[30rem] xs:h-[20rem] sm:h-72 bg-[#1A3899] mix-blend-multiply opacity-50 rounded-lg"></div>
                {
                    !isTeacher &&
                    <>
                        <div className="absolute w-full h-[30rem] xs:h-[20rem] sm:h-72 flex flex-col sm:flex-row justify-start">
                            <div className="hp-bar w-full px-4 mt-4">
                                <div className="relative">
                                    <div className="absolute h-4 w-full bg-white rounded-full"></div>
                                    <div style={{ "width": ((bossRemainingHealth / bossTotalHealth) * 100) + "%" }} className="absolute h-4 bg-[#F5CE40] rounded-full transition-all"></div>
                                    <p className='absolute top-6 font-medium text-white text-[.9rem]'>{bossRemainingHealth} / {bossTotalHealth}</p>
                                </div>
                            </div>
                            {
                                teamRemainingHealth !== undefined &&
                                <div className="hp-bar w-full px-4 mt-16 sm:mt-4">
                                    <div className="relative">
                                        <div className="absolute h-4 w-full bg-white rounded-full"></div>
                                        <div style={{ "width": ((teamRemainingHealth[router.query.team]["team_health"] / teamTotalHealth) * 100) + "%" }} className="absolute left-0 sm:left-auto sm:right-0 h-4 bg-[#5bbf67] rounded-full transition-all"></div>
                                        <p className='absolute top-6 left-0 sm:right-0 font-medium text-white text-[.9rem]'>{teamRemainingHealth[router.query.team]["team_health"]} / {teamTotalHealth}</p>
                                    </div>
                                </div>
                            }
                        </div>
                    </>
                }
                {
                    isTeacher &&
                    <div className="absolute w-full h-[30rem] xs:h-[20rem] sm:h-72 flex justify-center">
                        <div className="hp-bar w-full px-4 mt-4">
                            <div className="relative">
                                <div className="absolute h-4 w-full bg-white rounded-full"></div>
                                <div style={{ "width": ((bossRemainingHealth / bossTotalHealth) * 100) + "%" }} className="absolute h-4 bg-[#F5CE40] rounded-full transition-all"></div>
                                <p className='absolute top-6 font-medium text-white text-[.9rem]'>{bossRemainingHealth} / {bossTotalHealth}</p>
                            </div>
                        </div>
                    </div>
                }
                {
                    showBoss &&
                    <div className="absolute bottom-0 left-1/2 -translate-x-1/2 xs:-translate-x-0 xs:left-8 boss-sphinx" id={`${bossDead ? "boss-animation" : ""}`}>
                        <img className='h-40 sm:h-48 lg:h-52' src="/assets/sphinx.png" alt="sphix" />
                    </div>
                }
                <div className="absolute w-fulll right-1/2 translate-x-1/2 top-[45%] -translate-y-[45%] xs:right-8 xs:translate-x-0 xs:top-[60%] xs:-translate-y-[60%] sm:translate-x-0 sm:right-8 sm:top-1/2 sm:-translate-y-1/2 timer">
                    <div className="w-32 h-32 sm:w-36 sm:h-36 bg-white rounded-full flex items-center justify-center">
                        <h6 className='font-bold text-primary-400 text-[3rem] sm:text-[3.5rem]'>{initialTime}s</h6>
                    </div>
                </div>
                {
                    showFadeBoss &&
                    <div id='fade-big' className={` absolute top-1/2 -translate-y-1/2 left-[10%] sm:left-[25%] ${sumPerRound > 0 ? "bg-[#DB5353]" : "bg-[#DB5353]"} py-2 px-4 sm:py-4 sm:px-8 rounded-full`}>
                        <h6 className='font-bold text-white text-[1.5rem] sm:text-[2rem]'>{sumPerRound > 0 ? "-" + sumPerRound : "Miss"}</h6>
                    </div>
                }
                {
                    !isTeacher && showFadeBoss
                        ? <div id='fade-big' className={`${teamDamageList[router.query.team]["damagedInflicted"] < 0 ? "block" : "hidden"} absolute top-1/2 -translate-y-1/2 right-[10%] sm:right-[25%] ${sumPerRound > 0 ? "bg-[#5bbf67]" : "bg-[#5bbf67]"} py-2 px-4 sm:py-4 sm:px-8 rounded-full`}>
                            <h6 className='font-bold text-white text-[1.5rem] sm:text-[2rem]'>{teamDamageList[router.query.team]["damagedInflicted"] < 0 ? teamDamageList[router.query.team]["damagedInflicted"] : ""}</h6>
                        </div> : <></>
                }
                {
                    bossDead &&
                    <div id="defeated-animation" className="absolute w-full h-[30rem] xs:h-[20rem] sm:h-72 bg-[#bd5f48] rounded-lg flex flex-col justify-center items-center">
                        <h6 className='text-white font-bold text-center text-lg xs:text-2xl'>The boss has been defeated</h6>
                        <p className='mt-2 text-white text-sm xs:text-base'>Great works to all the teams!</p>
                    </div>
                }
                {
                    teamDead &&
                    <div id='defeated-animation' className="absolute w-full h-[30rem] xs:h-[20rem] sm:h-72 bg-[#bd5f48] rounded-lg flex flex-col justify-center items-center">
                        <h6 className='text-white font-bold text-center text-lg xs:text-2xl'>All teams has been defeated</h6>
                        <p className='mt-2 text-white text-sm xs:text-base'>Please try harder next time!</p>
                    </div>
                }
            </div>
            {
                !isTeacher
                    ? !showLoading
                        ? <div className='student-view'>
                            <div className="question-header w-full mt-6 mb-8 flex flex-col justify-center items-center ">
                                <p className='text-[.9rem] text-[#A4A4A4]'>Question {index + 1} of {currentGame.quizId.questionList.length}</p>
                                <h6 className='font-bold text-[1.4rem]'>{currentGame.quizId.questionList[index].question}</h6>
                            </div>
                            <div className="answer-list grid grid-cols-1 sm:grid-cols-2 gap-4">
                                {
                                    currentGame.quizId.questionList[index].answerList.map((answer, index) => {
                                        return (
                                            <div
                                                className={`
                                                ${isCorrect
                                                        ? "border-none bg-[#309A5180] text-white "
                                                        : isSelected
                                                            ? "border-none bg-[#3248da51] text-white"
                                                            : "border-[#DADADA] hover:bg-[#dadada3c] text-black"
                                                    }     
                                                border-2 px-6 py-8 rounded-lg hover:cursor-pointer flex justify-between
                                            `}
                                                key={index}
                                                onClick={() => sendAnswer(answer.body, answer.isCorrect)}

                                            >
                                                <p className='text-[1.3rem] font-bold'>{answer.body}</p>
                                                {isCorrect ? <ion-icon name="checkmark-outline" id="answer-check-white"></ion-icon> : <></>}
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        : <div className="mt-8 flex flex-col items-center">
                            <SandLoading />
                            <p className='text-center px-5'>Hang on tight! Prepare yourself for the next question</p>
                        </div>
                    : <></>

            }
            {
                isTeacher &&
                < div className='teacher-view flex flex-col-reverse lg:flex-row  gap-8 mt-4'>
                    <div className="teacher-qna w-full flex flex-col justify-between">
                        <div className="question-header w-full mb-8 flex flex-col justify-start items-start ">
                            <p className='text-[.9rem] text-[#A4A4A4]'>Question {index + 1} of {currentGame.quizId.questionList.length}</p>
                            <h6 className='font-bold text-[1.4rem]'>{currentGame.quizId.questionList[index].question}</h6>
                        </div>
                        <div>
                            <div className="answer-list grid grid-cols-1 sm:grid-cols-2 gap-4">
                                {

                                    currentGame.quizId.questionList[index].answerList.map((answer, index) => {
                                        return (
                                            <div
                                                className={`
                                                        ${isCorrect
                                                        ? "border-none bg-[#309A5180] text-white "
                                                        : isSelected
                                                            ? "border-none bg-[#3248da51] text-white"
                                                            : "border-[#DADADA]"
                                                    }     
                                                        border-2 px-6 py-6 rounded-lg hover:cursor-pointer flex justify-between
                                                    `}
                                                key={index}
                                            >

                                                <p className='text-[1.3rem] text-[#696969] font-bold'>{answer.body}</p>
                                                {isCorrect ? <ion-icon name="checkmark-outline" id="answer-check-white"></ion-icon> : <></>}
                                            </div>

                                        )
                                    })
                                }
                            </div>
                            {
                                isLast === false ? <div onClick={nextQuestion} className="submit bg-[#7384F6] mt-4 px-2 flex items-center justify-center py-4 rounded-md hover:box-border text-white transition-all hover:cursor-pointer hover:bg-primary-600">
                                    <p className='font-bold text-[.9rem]'>Next Question</p>
                                </div> : null
                            }
                        </div>
                    </div>
                    <div className="team-performance bg-[#7384F6] p-6 rounded-lg flex flex-col items-center">
                        <h6 className='font-bold text-white text-[1.4rem] mb-4'>Team Performance</h6>
                        <div className={`team-stat-list grid ${teamList.length !== 1 ? "xs:grid-cols-2 sm:grid-cols-4 lg:grid-cols-2" : "xs:grid-cols-1"}`}>
                            {
                                teamList && teamList.map((team, index) => {
                                    return (
                                        <div className="team-stat mb-2 mx-20 flex flex-col items-center relative" key={index}>
                                            <div className="answer text-white ">
                                                <h6 className='font-bold text-[3.5rem] mr-2'>{team.score ?? 0}</h6>
                                            </div>
                                            <div className="team-name-pill w-max max-w-[12rem] bg-white py-1 px-6  rounded-full">
                                                <h6 className='text-[#7384F6]  text-center font-bold text-[1.1rem]'>{team.name}</h6>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            }
        </div >
    )
}

export default PlayGame


export async function getServerSideProps({ req, res, query }) {
    let cookie = getCookie(req.headers.cookie)
    const accessToken = cookie.accessToken
    if (!accessToken) {
        return {
            redirect: {
                destination: '/log-in',
                permanent: false
            },
        }
    }

    console.log(query.id);
    const response = await axios.get(`/api/game/${query.id}`, {
        headers: {
            'authorization': `Bearer ${cookie.accessToken}`
        }
    })

    console.log(response.data)




    return {
        props: {
            game: response.data,
            isTeacher: cookie.role === "Teacher" ? true : false
        }, // will be passed to the page component as propsll be passed to the page component as props
    }


}