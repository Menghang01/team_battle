import React, { useState, useEffect } from 'react'
import TeamCard from '../components/team_card'
import { storeGame } from "../action/gameAction"
import { useDispatch, useSelector } from 'react-redux'
import { Popover } from '@headlessui/react';
import Router from "next/router"
import { useRouter } from 'next/router'
import { setLocale } from 'yup';
import SandLoading from '../components/lottie/loading';
import TeamCardAuto from '../components/team_card_auto';

const TeamSelectionAuto = () => {
    const router = useRouter()
    const dispatch = useDispatch()
    const socket = useSelector(state => state.socket.socket)

    const currentGame = useSelector((state) => state.game.current_game)
    const [nameList, setNameList] = useState([])
    const [teamList, setTeamList] = useState()
    const [isGenerated, setIsGenerated] = useState(false)

    useEffect(() => {
        console.log("DOG");
        if (currentGame === undefined) {
            Router.push('/teacher')
        }

        const tempTeam = []

        console.log(currentGame);

        Object.keys(currentGame["team"]).map((team_name, index) => {
            const temp = {
                team_name: team_name,
                team_members: [],
            }

            tempTeam.push(temp)
        })

        setTeamList(tempTeam)
    }, [])


    useEffect(() => {
        console.log("CURRENT_GAME")
        console.log(currentGame)
    }, [currentGame]);

    useEffect(() => {
        if (socket) {
            socket.on("player-added", (isLive, team, team_name) => {

                currentGame["team"] = team
                dispatch(storeGame({
                    ...currentGame
                }))

                let tempTeam = []

                Object.keys(currentGame["team"]).map((team_name, index) => {
                    const temp = {
                        team_name: team_name,
                        team_members: currentGame["team"][team_name],
                    }

                    tempTeam.push(temp)

                    console.log(team_name, currentGame["team"][team_name]);
                })
                setTeamList(tempTeam)
                console.log(currentGame);

            })

            socket.on("auto-mode-init-game", () => {
                console.log("auto")
                setNameList([])
            })

            socket.on("player-added-auto", (players_team) => {
                setNameList([...players_team])
            })

            socket.on("generated-team-done", (team) => {
                console.log(team)

                currentGame["team"] = team
                dispatch(storeGame({
                    ...currentGame
                }))

                let tempTeam = []

                Object.keys(currentGame["team"]).map((team_name, index) => {
                    const temp = {
                        team_name: team_name,
                        team_members: currentGame["team"][team_name],
                    }

                    tempTeam.push(temp)

                    console.log(team_name, currentGame["team"][team_name]);
                })
                setTeamList(tempTeam)
                setNameList([])
                setIsGenerated(true)
                console.log(currentGame);
            }) 
        }
    }, [socket]);

    const [loading, setLoading] = useState(false)

    const generateTeam = () => {
        socket.emit("generate-team-auto", nameList)
    }

    const startGame = () => {
        socket.emit("start-game", currentGame.quizId)
        setLoading(true)
        router.push(`/play?id=${currentGame._id}`)
    }

    return (
        currentGame &&
        <div className="main select-none">
            <div className="background w-screen h-screen bg-primary-600 relative overflow-y-scroll overflow-x-hidden" id='customscroll'>
                <img className='w-4/5 h-4/5 absolute -bottom-1/3 -left-1/3 z-1' src="/assets/radio_ring.svg" alt="doodle" />
                <h6 className='pl-5 pt-5 font-bold text-white text-[1.5rem]'>LOGO</h6>
                <div className="login-form w-screen h-screen absolute flex flex-col items-center justify-start">
                    <div className="join-code mb-8">
                        <h6 className='text-white font-bold text-[1.1rem] mb-4'>Enter the join code</h6>
                        <div className="join-code-container flex justify-center items-center bg-white rounded-md py-2 scale-110">
                            <h2 className='font-bold text-[1.8rem]'>{currentGame.pin}</h2>
                        </div>
                    </div>
                    <div className="student-name flex-col items-start w-full px-5">
                        <h6 className='font-bold text-white text-base mb-4'>Student names</h6>
                        <div className="bg-white w-full rounded-lg px-6 pt-6 pb-4">
                            <div
                                className="name-list-row flex flex-wrap"
                            >
                                {nameList.length !== 0 
                                ? nameList.map((name, index) => {
                                    return (
                                        <div
                                            className='bg-primary-400 text-white mr-2 mb-2 py-2 px-4 rounded-md'
                                        >
                                            <p>{name.firstName}</p>
                                        </div>
                                    )
                                })
                                : <p className='text-grey-600'>No one has joined the game yet</p>}
                            </div>
                        </div>
                    </div>
                            
                    <div className="team flex-col items-start w-full px-5 ">
                        <h6 className='font-bold text-white text-base my-4'>Teams</h6>
                        <div className="team-list grid grid-cols-1 xs:grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 gap-8 pr-4" id='customscroll'>
                            {
                                teamList && teamList.map((team, index) => {
                                    return (
                                        <TeamCardAuto teamInfo={{
                                            team_name: team.team_name,
                                            team_members: team.team_members
                                        }} key={index} />
                                    )
                                })
                            }
                        </div>
                    </div>
                    {
                        !isGenerated && 
                        <div onClick={generateTeam} className='text-primary-400 mt-20 py-2 px-6 rounded-md font-bold bg-white w-1/10 h-1/10 hover:cursor-pointer hover:border hover:border-white hover:bg-primary-600 hover:text-white transition-all'>Generate team</div>
                    }
                    {
                        isGenerated
                        ? !loading
                            ? <div onClick={startGame} className='text-primary-400 mt-20 py-2 px-6 rounded-md font-bold bg-white w-1/10 h-1/10 hover:cursor-pointer hover:border hover:border-white hover:bg-primary-600 hover:text-white transition-all'>Start game</div>
                            : <div className="mt-14">
                                <SandLoading />
                            </div>
                        : <></>
                    }
                    {/* <div onClick={nextQuestion} className='text-white bg-black w-1/10 h-1/10'>Next question</div> */}
                </div>

            </div>
        </div>
    )
}

export default TeamSelectionAuto