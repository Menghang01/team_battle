import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router'
import Cookies from 'cookies-js';



const JoinQuiz = () => {
    const socket = useSelector((state) => state.socket.socket)
    const router = useRouter()
    const [pin, setPin] = useState("123456")
    const onPinChange = e => setPin(e.target.value)

    const [userInfo, setUserInfo] = useState([])

    useEffect(() => {
        var retrievedUserInfo = localStorage.getItem('userInfo');
        console.log(retrievedUserInfo);
        if (retrievedUserInfo) {
            setUserInfo(JSON.parse(retrievedUserInfo))
        }
    }, [])





    const alertUser = (e) => {
        console.log("object");

        e.preventDefault();
        e.returnValue = "";
    };
    const handlePin = (e) => {
        setPin(e.target.value)
    }

    useEffect(() => {
        if (socket != null) {
            // socket.on("player-added", (isLive, team, team_name) => {
            //     if (isLive && team_name) {
            //         router.push({
            //             pathname: "/choose-team",
            //             query: {
            //                 team: JSON.stringify(team),
            //                 team_name: team_name
            //             }
            //         }, "/choose-team")
            //     }
            // })

            socket.on("player-added-auto", (waiting, user) => {
                let temp = JSON.parse(localStorage.getItem("userInfo"))
                // console.log(user.userType)
                console.log(temp, user, user._id === temp._id)

                if (user._id === temp._id) {
                    router.push({
                        pathname: "/choose-team",
                        query: {
                            waiting: waiting
                        }
                    }, "/choose-team")
                }
            })

            socket.on("player-added-manually", (waiting, user) => {
                let temp = JSON.parse(localStorage.getItem("userInfo"))
                // console.log(user.userType)
                console.log(temp, user, user._id === temp._id)
                if (user._id === temp._id) {
                    router.push({
                        pathname: "/choose-team",
                        query: {
                            waiting: waiting
                        }
                    }, "/choose-team")
                }

            })
        }

    }, [socket]);

    const joinGame = () => {
        console.log(pin);
        socket.emit("add-player", userInfo, socket.id, pin, "Spirit")
    }

    const onLogOut = () => {
        Cookies.expire("accessToken")
        Cookies.expire("id")
        Cookies.expire("role")

        localStorage.removeItem('userInfo')

        if (Cookies.get("accessToken") === undefined) {
            router.push('/log-in')
        }
    }

    return (
        <div className='grid grid-rows-6 sm:grid-rows-none sm:grid-cols-6 lg:grid-cols-2 h-screen'>
            <div className="doodle h-full w-full row-span-6 sm:col-span-2 lg:col-span-1">
                {/* <Image src="/assets/doodle.svg" layout='fill' sizes='100%'/> */}
                <img className='w-full h-full object-cover opacity-80' src="/assets/doodle.svg" alt="doodle" />
            </div>
            <div className="form h-full w-full row-span-6 sm:col-span-4 lg:col-span-1 bg-primary-600 text-white p-12 flex flex-col justify-between">
                <div className="flex items-center justify-between">
                    <div className="form-head font-bold">
                        <div className="form-welcome flex items-center space-x-3">
                            <img className='w-8 h-8 lg:h-10 lg:w-10 ' src="/assets/waving-hand.png" alt="waving" />
                            <h6 className='text-[1.5rem] lg:text-[2rem]'>Welcome, {userInfo.firstName}</h6>
                        </div>
                        {/* <h6 className='text-[1.5rem] lg:text-[2rem]'>{userInfo.firstName}</h6> */}
                        <p className='pt-2 text-[.9rem] lg:text-[1rem] font-thin'>Are you ready for battle?</p>
                    </div>
                    <div className="log-out" onClick={() => onLogOut()}>
                        <ion-icon id='menu-icon-big' name="log-out-outline"></ion-icon>
                    </div>
                </div>
                <div className="join-form">
                    <p className='pt-8 pb-4 lg:py-4 text-[1rem] font-medium'>Enter the game code below</p>
                    <input
                        type="number"
                        className='w-full py-3 px-5 text-black text-[1.5rem] lg:text-[2rem] font-bold rounded-md'
                        placeholder='123456'
                        onChange={onPinChange}
                    />
                    <div className="flex items-center justify-center">
                        <div onClick={joinGame} className="button w-fit mt-5 py-3 px-6 bg-white flex items-center justify-center rounded-md text-primary-600 text-[1rem] lg:text-[1.1rem] font-bold border hover:cursor-pointer hover:bg-primary-600 hover:border hover:border-white hover:text-white">Join Game</div>
                    </div>
                </div>
                <div className="logo h-40 flex justify-center items-end">
                    <h6 className='font-bold text-[1.8rem]'>LOGO</h6>
                </div>
            </div>
        </div>
    )
}

export default JoinQuiz


export async function getServerSideProps({ req, res }) {
    const accessToken = req.headers.cookie
    if (!accessToken) {
        return {
            redirect: {
                destination: '/log-in',
                permanent: false
            },
        }
    }
    return {
        props: {}, // will be passed to the page component as props
    }


}
