import React from 'react'
import { Droppable, Draggable } from "react-beautiful-dnd";

const TeamCard = ({ isChecked = false, teamInfo = {} }) => {
    return (
        <div className={`team-card text-black p-3 bg-white rounded-md box-border ${isChecked ? "border-4 border-green-600" : ""}`}>
            <div className='border-b-2 border-grey-500 pb-3 mb-5 flex justify-between'>
                <h6 className='font-bold'>{teamInfo.team_name ?? "Unnamed"}</h6>
                <div className={`circular-check w-6 h-6 rounded-full flex items-center justify-center ${isChecked ? "bg-green-600" : ""}`}>
                    <ion-icon name="checkmark-outline" id="check-white"></ion-icon>
                </div>
            </div>
            <Droppable droppableId={teamInfo.team_name} direction='horizontal'>
                {(provided) => (
                    <div 
                        className="member-list flex flex-wrap text-white"
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                    >
                        {
                            teamInfo.team_members && teamInfo.team_members.map((member, index) => {
                                return (
                                    <Draggable key={member._id} draggableId={member._id} index={index}>
                                        {(provided) => (
                                            <div 
                                                className="member-name bg-primary-400 mr-2 mb-2 py-2 px-4 rounded-md hover:cursor-pointer hover:scale-105 transition-all" 
                                                key={index}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                ref={provided.innerRef}
                                            >
                                                {/* <p>{member_name.userName}</p> */}
                                                <p>{member.firstName}</p>
                                            </div>
                                        )}
                                    </Draggable>
                                )
                            })
                        }
                        <div className="h-10"></div>
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </div>
    )
}

export default TeamCard