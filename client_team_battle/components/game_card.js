import React, { useEffect, useRef, useState } from 'react'
import Router, { useRouter } from 'next/router'
import Cookies from "cookies-js"
import { useDispatch } from "react-redux"
import { createGame } from '../action/gameAction'
const GameCard = ({ game = {}, isDraft = false }) => {
    const dispatch = useDispatch()
    const router = useRouter()
    const pushToGameMode = () => {
        console.log('dog');
        dispatch(createGame(games))
        Router.push("/game/select-game-mode")
    }

    // console.log(Cookies.get("id"))
    const [games, setGame] = useState({
        quizId: game._id,
        isLive: true
    })

    return (
        <div className='game-card border border-[#e9e9e9] rounded-md max-w-[18rem] mb-4 shadow-sm'>
            <img className='w-full h-44 object-cover' src="https://wallpaperaccess.com/full/186639.jpg" alt="game_card_cover" />
            <div className="game-card-content p-4 max-h-56 flex flex-col justify-between">
                <div className="content">
                    <div className="content-top flex justify-between items-center mb-3">
                        <h6 className='font-bold text-[1.1rem] sm:text-[1.2rem]'>{game.quizId.name}</h6>
                        <p className='text-[.7rem] text-[#7B7B7B]'>{new Date(game.date).toLocaleDateString('en-us', { year: "numeric", month: "long", day: "numeric" })}</p>
                    </div>
                    <div className="content-desc mb-4 h-14 text-ellipsis overflow-hidden">
                        <p className='text-[.7rem] sm:text-[.8rem] text-[#7B7B7B]'>{game.quizId.description}</p>
                    </div>
                </div>
                <div className="content-buttons flex flex-col sm:flex-row">
                    {
                        !isDraft &&
                        <>
                            {/* <div onClick={pushToGameMode} className="bg-primary-400  mr-2 w-full sm:w-fit mb-2 sm:mb-0 px-2 py-2 rounded-[.25rem] font-medium text-[.9rem] flex justify-center items-center hover:cursor-pointer hover:bg-primary-600 transition-all">
                                <p className='text-white text-[.8rem] text-center'>Start Game</p>
                            </div> */}
                            <div className="bg-white border border-primary-600 text-primary-600 px-3 py-2 rounded-md font-medium text-[.9rem] flex justify-center items-center hover:cursor-pointer hover:bg-primary-600 hover:text-white transition-all" onClick={() => router.push("/game/statistic/" + game._id)}>
                                <p className='text-[.8rem] text-center'>View Statistics</p>
                            </div>
                        </>
                    }
                    {
                        isDraft &&
                        <>
                            <div onClick={pushToGameMode} className="bg-primary-400 mr-2 w-full sm:w-fit px-4 py-2 mb-2 sm:mb-0 rounded-[.25rem] font-medium text-[.9rem] flex justify-center items-center hover:cursor-pointer hover:bg-primary-600 transition-all">
                                <p className='text-white text-[.8rem]'>Start Game</p>
                            </div>
                            <div className="bg-white border border-primary-600 text-primary-600 w-full sm:w-fit px-4 py-2 rounded-md font-medium text-[.9rem] flex justify-center items-center hover:cursor-pointer hover:bg-primary-600 hover:text-white transition-all" onClick={() => onToggleEditProfile()}>
                                <p className='text-[.8rem]'>Edit Game</p>
                            </div>
                        </>
                    }
                </div>
            </div>
        </div>
    )
}

export default GameCard