import React from 'react'

const QuizCard = ({
    isSelected = false, 
    index, 
    content = [],
    onQuestionSelect
}) => {
    return (
        <div 
            className={`quiz-card relative mb-2 w-[12.875rem] max-w-[12.875rem] lg:w-full lg:max-w-none lg:min-w-0 bg-white h-32 lg:h-36 rounded-md border-white hover:cursor-pointer flex items-center justify-center ${isSelected ? "border-primary-600 border-4" : "border hover:box-border hover:border-grey-500 hover:border-4"}`}
            onClick={() => onQuestionSelect(index)}
        >
            <p className='text-center font-bold'>{content.length != 0 ? content : "Type in your question"}</p>
            <div className={`card-number w-6 h-6 absolute left-2 bottom-2 flex justify-center items-center rounded-full ${isSelected ? "bg-primary-600" : "bg-grey-500"}`}>
                <p className='text-white font-bold'>{index + 1}</p>
            </div>
        </div>
    )
}

export default QuizCard