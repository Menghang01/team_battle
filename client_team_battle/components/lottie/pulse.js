import React from "react";
import Lottie from "react-lottie";
import animationData from "../../public/assets/lottie/pulse.json";

function PulseLoading({screenWidth}) {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
        },
    };

    return <Lottie 
        options={defaultOptions} 
        height={screenWidth <= 1024 ? 800 : 1000} 
        width={screenWidth <= 1024 ? 800 : 1000} 
    />;
}

export default PulseLoading;

