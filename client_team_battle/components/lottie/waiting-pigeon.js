import React from "react";
import Lottie from "react-lottie";
import animationData from "../../public/assets/lottie/waiting-pigeon.json";

function WaitingPigeon({screenWidth}) {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
        },
    };

    return <Lottie 
        options={defaultOptions} 
        height={screenWidth <= 1024 ? 200 : 250} 
        width={screenWidth <= 1024 ? 200 : 250} 
    />;
}

export default WaitingPigeon;

