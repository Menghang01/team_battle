import React from 'react'

const AnswerCard = ({
  answer, 
  index, 
  onSelectCorrectAnswer,
  onEachAnswerChange
}) => {
  return (
    <div className={`answer-card relative py-10 px-8 border-[#D9D9D9] flex justify-start items-center rounded-md box-border ${answer.isCorrect ? "border-green-600 border-[3px]" : "border-2"}`}>
        {/* <p className='font-bold text-center'>The answer may be way too long..............</p> */}
        <input 
          type="text" 
          placeholder={'Add answer ' + (index + 1)}
          className="w-full focus:outline-none font-medium" 
          onChange={(e) => onEachAnswerChange(e.target.value, index)}
          value={answer.body}
        />
        {
          answer.isCorrect 
          ? <div 
              className={`circular-check w-6 h-6 absolute right-2 top-2 rounded-full flex items-center justify-center bg-green-600 hover:cursor-pointer`}
              onClick={() => onSelectCorrectAnswer(index)}
            >
              <ion-icon name="checkmark-outline" id="check-white"></ion-icon>  
          </div> 
          : <div 
              className={`circular-check w-6 h-6 absolute right-2 top-2 border-2 border-[#D9D9D9] rounded-full hover:cursor-pointer hover:border-[#b2b2b2]`}
              onClick={() => onSelectCorrectAnswer(index)}
            >
          </div> 
        }
    </div>
  )
}

export default AnswerCard