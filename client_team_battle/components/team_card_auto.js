import React from 'react'

const TeamCardAuto = ({ isChecked = false, teamInfo = {} }) => {
    return (
        <div className={`team-card text-black p-3 bg-white rounded-md box-border ${isChecked ? "border-4 border-green-600" : ""}`}>
            <div className='border-b-2 border-grey-500 pb-3 mb-5 flex justify-between'>
                <h6 className='font-bold'>{teamInfo.team_name ?? "Unnamed"}</h6>
                <div className={`circular-check w-6 h-6 rounded-full flex items-center justify-center ${isChecked ? "bg-green-600" : ""}`}>
                    <ion-icon name="checkmark-outline" id="check-white"></ion-icon>
                </div>
            </div>
            
            <div className="member-list flex flex-wrap text-white">
                {
                    teamInfo.team_members && teamInfo.team_members.map((member, index) => {
                        return (
                                <div 
                                    className="member-name bg-primary-400 mr-2 mb-2 py-2 px-4 rounded-md hover:cursor-pointer hover:scale-105 transition-all" 
                                    key={index}
                                >
                                    <p>{member.firstName}</p>
                                </div>
                            )
                        })
                }
                <div className="h-10"></div>
            </div>
        </div>
    )
}

export default TeamCardAuto