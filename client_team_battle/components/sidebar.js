import Link from 'next/link'
import React from 'react'
import Cookies from 'cookies-js';
import { useRouter } from 'next/router';

const Sidebar = ({SideMenuTop, SideMenuBottom, page}) => {
    const router = useRouter()

    const onLogOut = () => {
        Cookies.expire("accessToken")
        Cookies.expire("id")
        Cookies.expire("role")

        localStorage.removeItem('userInfo')

        if (Cookies.get("accessToken") === undefined) {
            router.push('/log-in')
        }
    }
    
    return (
        <div className="">
            <div className="side-bar relative h-full w-20 sm:w-64 bg-[#FCFCFC] overflow-clip">
                <h6 className='absolute pl-3 sm:pl-9 pt-5 font-bold text-primary-600 text-[1.2rem] sm:text-[1.8rem]'>LOGO</h6>
                <div className="absolute mt-20 px-2 lg:px-5 side-bar-top w-full">
                    {
                        SideMenuTop.map((menu, index) => {
                            return (
                                <Link href={menu.page} key={index}>
                                    <div className={`${page == menu.page ? 'bg-primary-400 text-white font-bold rounded-lg mb-2' : 'text-[#7B7B7B] font-medium hover:scale-105 hover:font-medium transition-all'} menu-item flex items-center justify-center sm:justify-between w-full px-2 py-4 sm:px-5 sm:py-4 border-b border-white border-opacity-20 text-[.85rem] hover:cursor-pointer`} key={index}>
                                        <p className='hidden sm:block'>{menu.title}</p>
                                        <ion-icon id={`${page == menu.page ? 'menu-icon-white' : 'menu-icon'}`} name={menu.icon}></ion-icon>
                                    </div>
                                </Link>
                            )
                        })
                    }
                </div>
                <div className="absolute bottom-5 mt-20 sm:pl-5 px-2 lg:px-5 sm:px-0 side-bar-top w-full">
                    <Link href="/teacher/create-game">
                        <div className="bg-primary-400 sm:mb-8 sm:px-10 py-3 rounded-md font-medium w-full text-[.9rem] flex justify-center items-center hover:cursor-pointer hover:bg-primary-600 transition-all">
                            <p className='text-white hidden sm:block '>Create Game</p>
                            <div className="icon m-0 h-6 block sm:hidden">
                                <ion-icon name="add-circle-outline" id="add-circle-outline"></ion-icon>
                            </div>
                        </div>
                    </Link>
                    <div className="side-menu-bottom">
                        <Link href="/teacher/profile">
                            <div className="menu-item w-full flex justify-center sm:justify-start items-center my-6 text-white font-medium hover:cursor-pointer hover:scale-105 transition-all text-[.9rem] ">
                                <ion-icon id="menu-icon" name="person-circle-outline"></ion-icon>
                                <p className='text-[#7B7B7B] font-medium hidden sm:block sm:pl-6'>My Profile</p>
                            </div>
                        </Link>
                        <div 
                            className="menu-item w-full flex justify-center sm:justify-start items-center my-6 text-white font-medium hover:cursor-pointer hover:scale-105 transition-all text-[.9rem] "
                            onClick={onLogOut}
                        >
                            <ion-icon id="menu-icon" name="log-out-outline"></ion-icon>
                            <p className='text-[#7B7B7B] font-medium hidden sm:block sm:pl-6'>Log out</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Sidebar