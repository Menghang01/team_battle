import { io } from "socket.io-client";

const env = process.env.NODE_ENV
export const socket = io(env === 'development' ? 'localhost:3001' : '139.59.101.71:3001');