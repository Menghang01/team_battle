import Link from 'next/link'
import React from 'react'
import Sidebar from '../components/sidebar'
import { getCookie } from '../utils/helper';

const TeacherLayout = ({children, page="", showSideBar = true}) => {

    const SideMenuTop = [
        {
            icon: 'home-outline',
            title: 'Dashboard',
            page: '/teacher'
        },
        {
            icon: 'extension-puzzle-outline',
            title: 'Your Games',
            page: '/teacher/games'
        },
    ]

    const SideMenuBottom = [
        {
            icon: 'person-circle-outline',
            title: 'My Profile',
            page: '/teacher/profile'
        },
        {
            icon: 'log-out-outline',
            title: 'Log out',
            page: 'log-out'
        },
    ]

    return (
        <div className='teacher-layout flex h-screen w-screen'>
            <Sidebar 
                page={page}
                SideMenuTop={SideMenuTop}
                SideMenuBottom={SideMenuBottom}
            />
            <div className="w-full bg-[#ffffff]">
                {children}
            </div>
        </div>
    )
}

export default TeacherLayout

export async function getServerSideProps({ req, res }) {
    let cookie, role

    if (req.headers.cookie !== undefined) {
        cookie = getCookie(req.headers.cookie)
        role = cookie.role
    } else {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }

    if (role !== "Teacher" ) {
        return {
            redirect: {
                destination: '/join-quiz',
                permanent: false
            },
        }
    }

    return {
        props: {}, // will be passed to the page component as props
    }
}