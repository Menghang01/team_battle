module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layout/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'white': '#ffffff',
      'black': "#000000",
      'primary': {
        400: '#596DF2',
        600: '#3248DA'
      },
      'green': {
        400: '#E6F4E7',
        600: '#309A51'
      },
      'blue': {
        400: '#BEE0FF',
        600: '#5EB1FF',
      },
      'yellow': {
        400: '#FFEF99',
        600: '#F2D11C'
      },
      'orange': {
        400: '#FFBF97',
        600: '#FF9048'
      },
      'grey': {
        400: '#F5F5F5',
        500: '#D9DBE5',
        600: '#A8A8A8',
      }
    },
    screens: {
      'xs': '450px',
      // => @media (min-width: 450px) { ... }
      
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }
    },
    fontFamily: {
      sans: ['DM Sans', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
  },
  plugins: [],
}