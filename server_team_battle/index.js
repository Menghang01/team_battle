const express = require('express');
const app = express();



require('dotenv').config()

require('./startup/mongoose')();
require('./startup/route')(app);


const port = process.env.PORT || 8000


app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
})
