// const { instrument } = require("@socket.io/admin-ui")


// const io = require("socket.io")(3001, {
//     cors: {
//         origin: ["http://localhost:3000", "https://admin.socket.io/#/sockets"],
//     },
// })
const { instrument } = require("@socket.io/admin-ui")

const environment = process.env.NODE_ENV

const io = require("socket.io")(3001, {
    cors: {
        // origin: [
        //     environment === "development"
        //         ? process.env.LOCAL_HOST_URI
        //         : process.env.PROD_HOST_URI,
        //     "http://localhost:3000",
        //     "https://admin.socket.io/#/sockets"
        // ],
        origin: '*',
    },
})


let index = 0

let waiting_players = []

let team_status = {}



let answer_count_per_round = 0

let game = {}

let team_index = ["Spirit", "Water", "Fire", "Wind"]

let isLastQuestion

let player_team = {}

let team_answer = {}

let total_member_count = 0

let server = {
    "123": {
        isLive: false,
        questionIndex: 0,
        teamStatistic: {
            "Spirit": {
                answer: [
                    {
                        playerAnswer: [
                        ],
                        damagedInflicted: 0
                        // dmg per question
                    },
                ],
                totalDamageInflicted: 0
            }
        },

        team: {
            "Spirit": [],
            "Water": [],
            "Fire": [],
            "Wind": [],
        }
    },
}

io.on("connection", (socket) => {
    socket.on("disconnect", (reason) => {
        console.log("Socket " + socket.id + " was disconnected")

    })



    socket.on("init-game", (newGame, cb) => {
        game = JSON.parse(JSON.stringify(newGame))
        index = 0
        waiting_players = []
        team_answer = {}
        total_member_count = 0
        answer_count_per_round = 0
        isLastQuestion = false
        team_status = {}
        hostId = socket.id

        server[game.pin] = {
            ...game,
            teamStatistic: {

            },
            // boss_health: game.quizId.questionList.length * 200,
            boss_health: 0,
            team: {

            }

        }
        socket.join(game.pin)
        let arr = team_index.slice(0, game.team_count)
        team_members_count = game.team_count
        let team_stat
        for (const team of arr) {
            server[game.pin].team[team] = []
            team_answer[team] = []
            team_stat = server[game.pin].teamStatistic
            team_stat[team] = {
                answer: [
                    ...game.quizId.questionList.map(e => ({
                        playerAnswer: [


                        ],
                        damagedInflicted: 0
                    }))

                ],
                totalDamageInflicted: 0
            }


        }
        if (game.allocation_setting === "manual") {
            console.log("pin-manual");
            setTimeout(() => {
                io.emit("manual-mode-init-game")
            }, 500);
        }


        cb(server[game.pin])
        console.log(
            "Host with id " + socket.id + " started game and joined room: " + game.pin
        )
    })


    socket.on("add-player-manually", (user, selected_team) => {
        let index = waiting_players.findIndex(e => e._id)
        server[game.pin].team[selected_team] = []
        team_answer[selected_team] = []
        server[game.pin].team[selected_team].push(
            {
                ...
                waiting_players[index]
            }
        )

        team_status = {
            ...server[game.pin].team
        }

        socket.join(game.pin)

        waiting_players.splice(index, 1)
        // console.log(server[game.pin].team[selected_team])
        total_member_count++
        io.emit('manually-added-successfully', user, selected_team)
    })



    socket.on("add-player", (user, socketId, pin) => {
        if (game) {
            if (game.pin === pin) {
                if (game.allocation_setting === "auto") {
                    socket.join(game.pin)
                    isDuplicate = false
                    random_team_name = team_index[Math.floor(Math.random() * game.team_count)];

                    console.log(game);
                    if (game.allocation_setting === "auto") {
                        server[pin].team[random_team_name].forEach((e) => {
                            if (e.socketId == socketId)
                                isDuplicate = true
                        })

                        if (!isDuplicate) {
                            server[pin].team[random_team_name].push({
                                ...user,
                                socketId
                            })
                            player_team[socketId] = random_team_name
                        }
                    }

                    total_member_count++
                    console.log(server[game.pin].team)
                    team_status = {
                        ...server[game.pin].team
                    }

                    // console.log("before", team_status)
                    io.emit("player-added", server[game.pin].isLive, server[game.pin].team, random_team_name)
                } else {

                    waiting_players.push({
                        ...user,
                        socketId,
                    })

                    io.emit("player-added-manually", waiting_players, user)
                }
            }

        }

    })

    socket.on("time-up", (cb) => {
        console.log(cb);
        // io.emit("game-stat", server[game.pin])
        io.to(game.pin).emit("game-stat", server[game.pin])
    })

    socket.on("game-joined", (cb) => {

        cb(server[game.pin], team_status)
    })

    socket.on("start-game", (newQuiz) => {

        quiz = JSON.parse(JSON.stringify(newQuiz))
        timer = server[game.pin].quizId.questionList[index].answerTime
        server[game.pin].true_dmg = parseFloat(((server[game.pin].quizId.questionList.length * total_member_count) / game.team_count).toFixed(1))
        server[game.pin].boss_health = server[game.pin].true_dmg * game.team_count * server[game.pin].quizId.questionList.length
        for (const key in server[game.pin].team) {
            server[game.pin].team[key].team_health = server[game.pin].boss_health / game.team_count
        }

        console.log(server[game.pin])
        io.emit("move-to-game-page", server[game.pin])
    })


    socket.on("next-question", (cb) => {
        index += 1
        io.emit("next-question-screen", index)
        timer = 10
        answer_count_per_round = 0
        for (const key in team_answer) {
            console.log(key)
            server[game.pin].teamStatistic[key].answer.playerAnswer = team_answer[key]
            team_answer[key] = []
        }
        console.log("server stats", server[game.pin].teamStatistic)
        console.log("check-index", isLastQuestion, index, server[game.pin].quizId.questionList.length)
        isLastQuestion = index === server[game.pin].quizId.questionList.length - 1
        console.log(index === server[game.pin].quizId.questionList.length)
        // console.log("index", index)
        // console.log("questionList", server[game.pin].quizId.questionList.length - 1)
        cb(isLastQuestion)
    })




    socket.on("send-answer-to-host", (data, score) => {
        let player = getPlayer(socket.id)
        socket.to(game.pin).emit("get-answer-from-player", data, leaderboard._id, score, player)
    })


    socket.on("answer-question", (answer) => {
        answer_count_per_round++
        team_answer[answer.team].push(
            answer
        )

        const team = team_index.slice(0, game.team_count)
        console.log(total_member_count);
        if (answer_count_per_round == total_member_count) {
            for (const key of team) {
                console.log("key", key)
                console.log(team_answer);
                console.log("team_answer", team_answer[key])
                const result = checkIfCanHit(key)
                io.emit('attack', result)
            }


        }
        console.log("answer-question", index, server[game.pin].quizId.questionList.length)
        // if (index === server[game.pin].quizId.questionList.length - 1) {
        //     io.emit('end-game-stats', server[game.pin])
        // }

    })









})




const checkIfCanHit = (team_name) => {
    // console.log("fuck", team_name, team_status)
    let result = true
    let inTime = true
    let overTime = []
    let wrong_answer_timer = []
    let team_member_anwer_count = 0
    let multiplier = 0
    let status = "Attack"

    console.log("team_answer", team_answer)
    console.log("team_status", team_status)
    console.log("team_name", team_name)

    let time = 0
    for (const answer of team_answer[team_name]) {
        time += parseInt(answer.time_taken)
        multiplier += 10 - answer.time_taken
        team_member_anwer_count++
        if (answer.isCorrect == false) {
            result = false
            wrong_answer_timer.push(answer.time_taken)
        }

        if (answer.time_taken > 5) {
            inTime = false
            overTime.push(answer.time_taken)
        }
    }
    console.log("team_status_in_answerquestion", team_status)

    console.log(server[game.pin].team[team_name]);


    if (team_member_anwer_count < team_status[team_name].length) {
        time += 10 * (team_status[team_name].length - team_member_anwer_count)
        overTime.push(10)
    }

    if (result === true && inTime === true) {
        console.log(multiplier)
        server[game.pin].teamStatistic[team_name].answer[index].average_time = time / team_status[team_name].length
        server[game.pin].teamStatistic[team_name].answer[index].damagedInflicted = Math.floor(multiplier / team_status[team_name].length / 10 * 100)
        server[game.pin].teamStatistic[team_name].damagedInflicted = Math.floor(multiplier / team_status[team_name].length / 10 * 100)
        server[game.pin].teamStatistic[team_name].totalDamageInflicted += Math.floor(multiplier / team_status[team_name].length / 10 * 100)
    }

    else if (result === true && inTime === false) {
        server[game.pin].teamStatistic[team_name].answer[index].average_time = time / team_status[team_name].length
        server[game.pin].teamStatistic[team_name].answer[index].damagedInflicted = 0
        server[game.pin].teamStatistic[team_name].damagedInflicted = 0
        server[game.pin].teamStatistic[team_name].totalDamageInflicted += 0
    }
    else if (result === false && time >= 18) {

        server[game.pin].teamStatistic[team_name].answer[index].average_time = time / team_status[team_name].length
        server[game.pin].teamStatistic[team_name].answer[index].damagedInflicted = Math.floor(time / 3 / 10 * 1.5 * 100) * (-1)
        server[game.pin].teamStatistic[team_name].damagedInflicted = Math.floor(time / 3 / 10 * 1.5 * 100) * (-1)
        server[game.pin].teamStatistic[team_name].totalDamageInflicted += Math.floor(time / 3 / 10 * 100) * (-1)
    }
    else if (result === false && time < 18) {
        const min_wrong_team = Math.min.apply(Math, wrong_answer_timer)
        server[game.pin].teamStatistic[team_name].answer[index].average_time = time / team_status[team_name].length
        server[game.pin].teamStatistic[team_name].answer[index].damagedInflicted = Math.floor((10 - min_wrong_team) / 10 * 100) * (-1)
        server[game.pin].teamStatistic[team_name].damagedInflicted = Math.floor((10 - min_wrong_team) / 10 * 100) * (-1)
        server[game.pin].teamStatistic[team_name].totalDamageInflicted += Math.floor((10 - min_wrong_team) / 10 * 100) * (-1)

    }


    console.log("ANSWER")
    console.log("stat", server[game.pin].teamStatistic[team_name])

    return {
        canHit: result,
        status: status,
        team_score: {
            ...server[game.pin].teamStatistic[team_name],
            team_name: team_name
        }
    }
}


const team_name = ["Spirit", "Water", "Fire", "Wind"]


instrument(io, { auth: false })