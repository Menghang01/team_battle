const cookieParser = require('cookie-parser')
const cors = require('cors')
const express = require('express');
const quiz = require('../route/quiz');
const user = require('../route/user');
const game = require('../route/game');
const bodyParser = require('body-parser');
const {
    authenticateToken,
} = require("../middleware/auth");
const multer = require('multer');
const forms = multer();
const authentication = require('../route/authentication');
module.exports = (app) => {
    app.use(cors({
        // origin: 'http://localhost:3000',
        'origin': '*',
    }))
    app.use(bodyParser.json());
    app.use(forms.array());

    app.use(bodyParser.urlencoded({ extended: true }));

    app.use(cookieParser())
    app.use("/api/", authentication);

    app.use("/api/user", user);
    app.use(authenticateToken);

    app.use("/api/quiz", quiz);
    app.use("/api/game", game);

}