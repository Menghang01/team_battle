const mongoose = require("mongoose")
require('dotenv').config();

const environment = process.env.NODE_ENV

module.exports = function () {
    return mongoose.connect(
        environment === "development"
        ? process.env.LOCAL_MONGO_URI
        : process.env.PROD_MONGO_URI,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        })
        .then(() => console.log("Connected to database"))

}