const mongoose = require("mongoose")
const Game = require("../models/game")

const createGame = async (req, res) => {
    const { quizId, isLive, team_count, member_per_team = 3, allocation_setting, game_mode, team = [], teamStatistic = {} } = req.body

    const game = new Game({
        hostId: req.user.id,
        quizId,
        date: new Date().toISOString(),
        pin: Math.floor(100000 + Math.random() * 900000),
        allocation_setting,
        game_mode,
        team_count,
        isLive,
        team,
        teamStatistic,
        member_per_team
    })


    try {
        let newGame = await game.save()
        newGame = await newGame.populate("quizId").execPopulate()
        res.status(201).json(newGame)
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
}

const getGames = async (req, res) => {
    try {
        const games = await Game.find()
        res.status(200).send(games)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const getGame = async (req, res) => {
    let game

    try {
        game = await Game.findById(req.params.id).populate("quizId")
        if (game == null) {
            return res.status(404).json({ message: "Game not found" })
        }
        res.json(game)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const deleteGame = async (req, res) => {
    const { id } = req.params
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).send(`No game with id: ${id}`)
    }

    try {
        await Game.findByIdAndRemove(id)
        res.json({ message: "Game deleted succesfully" })
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
}

const updateGame = async (req, res) => {
    const { id } = req.params
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(404).send(`No game with id: ${id}`)
    }

    const { quizId, isLive, date, team_count, member_per_team = 3, allocation_setting, game_mode, team, teamStatistic } = req.body


    // const game = new Game({
    //     hostId: req.user.id,
    //     quizId,
    //     date,
    //     pin: "123456",
    //     allocation_setting,
    //     game_mode,
    //     team_count,
    //     isLive,
    //     team,
    //     teamStatistic,
    //     member_per_team
    // })

    try {
        const updatedGame = await Game.findByIdAndUpdate(id, {
            teamStatistic: teamStatistic,
            team: team
        }, { new: true })
        res.json(updatedGame)
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
}


const getGameTeacher = async (req, res) => {
    const { teacherId } = req.params
    let game
    try {
        game = await Game.find({ hostId: teacherId }).sort({ 'date': -1 }).populate('quizId').limit(5)
        return res.send(game)
    } catch (e) {
        console.log(e)
    }
}

const addPlayer = async (req, res) => {
    const { gameId } = req.params
    const { playerId } = req.body

    let game

    try {
        game = await Game.findById(gameId)
        game.playerList.push(playerId)
        const updatedGame = await game.save()
        res.send(updatedGame)
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
}

module.exports = { createGame, getGames, getGame, deleteGame, updateGame, addPlayer, getGameTeacher }
