require("dotenv").config();

const mongoose = require("mongoose");
const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const login = async (req, res) => {
    const user = await User.findOne({ mail: req.body.mail });
    if (user == null) {
        return res.status(400).send("Cannot find user");
    }
    try {
        if (await bcrypt.compare(req.body.password, user.password)) {
            const accessToken = generateAccessToken({
                userName: user.userName,
                id: user._id,
                mail: user.mail
            });

            const refreshToken = jwt.sign(
                { userName: user.userName, id: user._id, mail: user.mail },
                process.env.REFRESH_TOKEN_SECRET,
                { expiresIn: "240h" }
            );

            res.json({
                result: user,
                accessToken: accessToken,
                refreshToken: refreshToken,
            });
        } else {
            res.send("Not allowed");
        }
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const generateAccessToken = (userData) => {
    return jwt.sign(userData, process.env.ACCESS_TOKEN_SECRET, {
        expiresIn: "240h",
    });
};

module.exports = { login };
