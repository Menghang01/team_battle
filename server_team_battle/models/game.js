const mongoose = require("mongoose");

const gameSchema = new mongoose.Schema({
    hostId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    quizId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Quiz",
    },
    pin: {
        type: String,
    },
    isLive: {
        type: Boolean,
        default: false,
    },
    teams: [
        {
            playerList: [
                {
                    // type: mongoose.Schema.Types.ObjectId,
                    // ref: "User",
                },
            ]
        }
    ],
    gameMode: {
        type: String
    },
    date: {
        type: Date,
        required: true,
        default: Date.now,
    },

    allocation_setting: {
        type: String,
        required: true,
    },

    teamStatistic: {

    },
    team: {

    },
    team_count: {
        type: Number,
        required: true
    },
    member_per_team: {
        type: Number,
        required: true
    },
    boss_health: {
        type: Number
    }

});

module.exports = mongoose.model("Game", gameSchema);
