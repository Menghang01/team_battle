const mongoose = require("mongoose")

const quizSchema = new mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String },
    creatorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },

    dateCreated: { type: Date, default: new Date() },
    
    questionList: [
        {
            questionType: {
                type: String,
                enum: ["Quiz"],
                required: true,
            },
            answerTime: {
                type: Number,
                min: 5,
                max: 90,
            },
            question: {
                type: String,
                required: true,
            },
            answerList: [
                {
                    name: { type: String },
                    body: { type: String },
                    isCorrect: { type: Boolean },
                },
            ],
            questionIndex: { type: Number, required: true },

        },
    ],
})

module.exports = mongoose.model("Quiz", quizSchema)
