const { instrument } = require("@socket.io/admin-ui")


const io = require("socket.io")(3001, {
    // cors: {
    //     origin: ["http://localhost:3000", "https://admin.socket.io/#/sockets"],
    // },
    cors: {
        origin: '*',
    }
})

let game
let leaderboard

let questionIndex = 0

let playerAnswer = {
    "Spirit": [
        {
            playerAnswer: [

            ],
            damagedInflicted: 0
        },

    ]
}

let team = {
    "Spirit": [],
    "Water": [],
    "Fire": [],
    "Wind": [],
}

const addPlayer = (userName, socketId) => {
    !players.some((player) => player.socketId === socketId) &&
        players.push({ userName, socketId })
}

const getPlayer = (socketId) => {
    return players.find((player) => player.socketId === socketId)
}

io.on("connection", (socket) => {
    socket.on("disconnect", (reason) => {

        console.log("Socket " + socket.id + " was disconnected")
        for (const key in team) {
            team[key].forEach((e, index) => {
                if (e.socketId === socket.id) {
                    team[key].splice(index, 1)
                }
            })
        }

        console.log(team);
    })

    socket.on("init-game", (newGame, newLeaderboard) => {
        game = JSON.parse(JSON.stringify(newGame))
        leaderboard = JSON.parse(JSON.stringify(newLeaderboard))
        socket.join(game.pin)
        hostId = socket.id
        console.log(
            "Host with id " + socket.id + " started game and joined room: " + game.pin
        )
    })


    socket.on("add-player", (user, socketId, pin, team_name) => {
        if (123 === pin) {

            isDuplicate = false

            team[team_name].forEach((e) => {
                if (e.socketId == socketId)
                    isDuplicate = true
            })

            if (!isDuplicate) {
                team[team_name].push({
                    ...user,
                    socketId
                })
            }

            io.emit("player-added", "hi", team)

        } else {
            cb("wrong", game._id)
        }
    })

    socket.on("start-game", (newQuiz) => {
        quiz = JSON.parse(JSON.stringify(newQuiz))
        console.log("Move players to game")
        console.log(game.pin)
        socket.to(game.pin).emit("move-to-game-page", game._id)
    })

    socket.on("question-preview", (cb) => {
        cb()
        socket.to(game.pin).emit("host-start-preview")
    })



    socket.on("attack", (damage) => {

    })

    socket.on("answer", (timer, isCorrect, user, question, team) => {

    })


    socket.on("start-question-timer", (time, question, cb) => {
        console.log("Send question " + question.questionIndex + " data to players")
        socket.to(game.pin).emit("host-start-question-timer", time, question)
        cb()
    })

    socket.on("send-answer-to-host", (data, score) => {
        let player = getPlayer(socket.id)
        socket.to(game.pin).emit("get-answer-from-player", data, leaderboard._id, score, player)
    })



})



const addPlayerToTeam = (team_mode, user, socketId, team = null) => {
    if (team_mode == "random") {
        const random = Math.floor(Math.random() * 4)
        team[team_name[random]].push(
            {
                ...user,
                socketId
            }
        )
    } else {
        const random = Math.floor(Math.random() * 4)
        team[team_name[random]].push(
            {
                ...user,
                socketId
            }
        )
    }
}

const team_name = ["Spirit", "Water", "Fire", "Wind"]


instrument(io, { auth: false })
