const express = require("express")
const router = express.Router()

const {
    createGame,
    getGames,
    getGame,
    updateGame,
    deleteGame,
    addPlayer,
    getGameTeacher
} = require("../controllers/GameController")

router
    .route("/")
    .get(getGames)
    .post(createGame)

router.route("/teacher/:teacherId").get(getGameTeacher)

router
    .route("/:gameId/players")
    .patch(addPlayer)

router
    .route("/:id")
    .get(getGame)
    .put(updateGame)
    .delete(deleteGame)

module.exports = router
